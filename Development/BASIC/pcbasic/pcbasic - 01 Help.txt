SYNOPSIS

pcbasic [program|package [output]] [OPTION] ...

POSITIONAL ARGUMENTS

Positional arguments must come before any options, must not start with
a dash -. Any positional arguments that follow options will be
ignored.

program
	If a .BAS program is specified as the first positional argument, it
	will be run. The --run, --load and --convert options override this
	behaviour.

package
	If a zipfile package or directory is specified as the first positional
	argument, any contained configuration file PCBASIC.INI will be loaded;
	usually, it will run a program file in the package. All other command-
	line options will override the package configuration file, note in
	particular the potential of the --run, --load and --convert options to
	alter the behaviour of the package.

output
	If a second positional argument is specified, it sets the output file
	for file format conversion. This argument is ignored unless the
	--convert option is given.

OPTIONS

--allow-code-poke[=True|=False]
	Allow programs to POKE into code memory.

--aspect=x,y
	Set the display aspect ratio to x:y. Only has an effect if combined
	with --interface=graphical.

-b
	Use the command-line interface. This is identical to --interface=cli.

--border=width
	Set the width of the screen border as a percentage from 0—100. The
	percentage refers to the total width of the borders on both sides as a
	fraction of the usable screen width. Only has an effect if combined
	with --interface=graphical.

--caption=title
	Set the title bar caption of the PC-BASIC window. Default title is PC-
	BASIC.

--cas1=type:value
	Attach a resource to the CAS1: cassette device. type:value can be

	WAV:wav_file
		Connect to the RIFF Wave file wav_file with data modulated in IBM PC
		cassette format.

	CAS:cas_file
		Connect to the PCE/PC-BASIC CAS tape image cas_file.

--codepage=codepage_id[:nobox]
	Load the specified codepage. The codepage determines which characters
	are associated to a given character byte or, in the case of double-
	byte codepages, two character bytes. The available codepages are
	stored in the codepage/ directory; by default, these are: 437 720 737
	775 806 850 851 852 853 855 856 857 858 860 861 862 863 864 865 866
	868 869 874 932 934 936 938 949 950 1258 alternativnyj armscii8a
	big5-2003 big5-hkscs georgian-academy georgian-ps iransystem iscii-as
	iscii-be iscii-de iscii-gu iscii-ka iscii-ma iscii-or iscii-pa iscii-
	ta iscii-te kamenicky koi8-r koi8-ru koi8-u mazovia mik osnovnoj
	pascii ruscii russup3 russup4ac russup4na viscii . See the list of
	codepages in the User's Guide for details. The specifier nobox
	disables box-drawing recognition for double-byte character set code
	pages. By default, sequences of box-drawing characters are recognised
	by an algorithm that isn't as smart as it thinks it is, and displayed
	as box drawing rather than as DBCS characters. If nobox is set, they
	will be displayed as DBCS.

--config=config_file
	Read a configuration file. The system default configuration is always
	read first, but any preset group of options in a configuration file
	replaces the whole equivalent default preset group.

--com1=type:value
	Attach a resource to the COM1: serial device. type:value can be one of
	the following.

	PORT:device_name
		Connect to a serial device. device_name can be a device name such as
		COM1 or /dev/ttyS0 or a number, where the first serial port is number
		0.

	SOCKET:host:socket
		Connect to a TCP socket on a remote or local host.

	RFC2217:host:socket
		Connect using the RFC2217 protocol to a TCP socket on a remote or
		local host.

	STDIO:[CRLF]
		Connect to standard I/O of the calling shell. If CRLF is specified,
		PC-BASIC replaces CR characters with LF on its output and LF with CR
		on its input. This is more intuitive on Unix shells. When using a Unix
		console, you should use stty -icanon to enable PC-BASIC to read input
		correctly.

	If this option is not specified, the COM1: device is unavailable.

--com2=type:value
	Attach a resource to the COM2: serial device. See --com1.

--convert={A|B|P}
	Convert program to one of the following formats:

	A
		Plain text

	B
		Tokenised

	P
		Protected

	If output is not specified, write to standard output. If program is
	not specified, use the argument of --run or --load. If none of those
	are given, read from standard input. Overrides --resume, --run and
	--load.

--mouse-clipboard[=True|=False]
	Enable clipboard operations with the mouse. If True (default), select
	text with the left mouse button to copy and paste with the middle
	mouse button.

--ctrl-c-break[=True|=False]
	If False, follow GW-BASIC behaviour where Ctrl+C breaks AUTO and INPUT
	but not program execution or LIST. If True, treat Ctrl+C exactly like
	Ctrl+Break and Ctrl+Scroll Lock when --interface=graphical. With
	--interface={text|cli}, Ctrl+C is always treated like Ctrl+Break.
	Default is True.

--current-device={CAS1|@|A|B ... |Z}
	Set the current device to the indicated PC-BASIC drive letter or CAS1
	for the cassette device. The device chosen should be mounted to an
	actual location using --mount (or --cas1 if the cassette device is
	chosen).

--debug[=True|=False]
	Developer option - use only if you know what you're doing. Enable
	debugging extension.

--dimensions=x,y
	Set window dimensions to x by y pixels. This overrides
	--scaling=native and --aspect. Only has an effect if combined with
	--interface=graphical.

-d --double[=True|=False]
	Enable double-precision transcendental math functions. This is
	equivalent to the /d option in GW-BASIC.

-e=statement[:statement ...] --exec=statement[:statement ...]
	Execute BASIC statements. The statements are executed after loading
	any program but before entering into direct mode or running it.
	Multiple statements can be entered by separating them with colons :.
	These will be executed as if they were entered as separate statements,
	not as a single compound statement; even if statements such as GOTO
	can be included, the following statements will be executed. If a : is
	intended to be part of a statement, the statement must be quoted with
	single quotes '. If your calling shell interprets such quotes, you
	should properly escape them.

--extension=module_name[,module_name ... ]
	Developer option - use only if you know what you're doing. Load
	extension module(s).

--font=font_name[,font_name ... ]
	Use the specified fonts for the interface. The last fonts specified
	take precedence, previous ones are fallback. Default is
	unifont,univga,freedos (i.e. the freedos font has preference). The
	available fonts are stored in font/. By default, the following fonts
	are available: unifont univga freedos cga mda vga olivetti tandy1
	tandy2 . See the list of fonts in the User's Guide for details.

--fullscreen[=True|=False]
	Fullscreen mode. Only has an effect if combined with
	--interface=graphical.

-h --help
	Show a usage message and exit.

--hide-listing=line_number
	Disable listing and saving to plain text of lines beyond line_number,
	as in GW-BASIC beyond 65530. Use with care as this allows execution of
	hidden lines of code. Default is to list all lines.

--hide-protected[=True|=False]
	Disable listing and saving to plain text of protected files, as in GW-
	BASIC. Use with care as this allows execution of hidden lines of code.

--input={input_file|{STDIO|STDIN}[:RAW]}
	Retrieve keyboard input from input_file, except if KYBD: is read
	explicitly. Input from KYBD: files is always read from the keyboard,
	following GW-BASIC behaviour. If input_file is STDIO: or STDIN:,
	keyboard input will be read from standard input. If RAW is specified,
	input will be treated as codepage bytes. If not, it will be treated as
	the locale's encoding (probably UTF-8).

--interface=[none|cli|text|graphical]
	Choose the type of interface. Not all interfaces will be available on
	all systems. The following interface types may be available:

	none
		Filter for use with pipes. Also -n.

	cli
		Command-line interface. Also -b.

	text
		Text interface. Also -t.

	graphical
		Graphical interface.

	sdl2
		SDL2 graphical interface.

	pygame
		PyGame graphical interface (deprecated, please use SDL2 instead).

	ansi
		ANSI text interface.

	curses
		NCurses text interface.

	The default is graphical.

-k=keystring --keys=keystring
	Insert the keystring into the keyboard buffer. keystring may contain
	escape codes such as \r for return, \n for line feed and \xXX to enter
	CHR$(&HXX). keystring may contain e-ASCII codes to indicate keypresses
	that do not have a regular character encoding. For example, \0\x0F
	indicates Shift+Tab.

-l=program --load=program
	Start in direct mode with the BASIC program loaded.

--logfile=log_file
	Write error and warning messages to log_file instead of stderr.

--lpt1=type:value
	Attach a resource to the LPT1: parallel device. type:value can be

	PRINTER:[printer_name][:trigger]
		Connect to a Windows, LPR or CUPS printer. If printer_name is not
		specified, the default printer is used. The printer is activated when
		a file open to it is closed, a program terminates or PC-BASIC exits.
		Note that, unlike LPT1:, printers connected to LPT2: or LPT3: do not
		get activated when a program terminates. If specified, trigger sets an
		additional trigger to activate the printer:

		line
			After every line break.

		page
			After every page break.

		close
			No additional trigger

		The default behaviour is that of close.

	FILE:file_name
		Connect to any file or device such as /dev/stdout on Unix or LPT1 on
		Windows.

	STDIO:[CRLF]
		Connect to standard output of the calling shell. If CRLF is specified,
		PC-BASIC replaces CR characters with LF on its output. This is more
		intuitive on Unix shells.

	PARPORT:port_number
		Connect to a Centronics parallel port, where port_number is 0 for the
		first parallel port, etc. PARPORT only works with physical parallel
		ports; for example, a Windows printer or other device mapped with NET
		USE LPT1: can only be attached with FILE:LPT1

	If this option is not specified, LPT1: is connected to the default
	printer.

--lpt2=type:value
	Attach a resource to the LPT2: parallel device. See --lpt1. If this
	option is not specified, LPT2: is unavailable.

--lpt3=type:value
	Attach a resource to the LPT3: parallel device. See --lpt1. If this
	option is not specified, LPT3: is unavailable.

-f=number_of_files --max-files=number_of_files
	Set maximum number of open files to number_of_files. This is
	equivalent to the /f option in GW-BASIC. Default is 3.

--max-memory=max_memory[,basic_memory_blocks]
	Set the maximum size of the data memory segment to max_memory and the
	maximum size of the data memory available to BASIC to
	basic_memory_blocks*16. In PC-BASIC, the minimum of these values is
	simply the data memory size; the two values are allowed for
	compatibility with the /m option in GW-BASIC.

-s=record_length --max-reclen=record_length
	Set maximum record length for RANDOM files to record_length. Default
	is 128, maximum is 32767. This is equivalent to the /s option in GW-
	BASIC.

--monitor={rgb|composite|green|amber|grey|mono}
	Sets the monitor type to emulate. Available types are:

	rgb
		RGB colour monitor (default).

	composite
		Composite colour monitor.

	green
		Green-tinted monochrome monitor.

	amber
		Amber-tinted monochrome monitor.

	grey
		Greyscale monochrome monitor.

	mono
		Green-tinted monochrome monitor (same as green).

	On SCREEN 2 with --video={pcjr|tandy|cga|cga_old}, --monitor=composite
	enables (crude) colour artifacts.

--mount=drive:path,[drive:path ... ]
	Assign the path path to drive letter drive:. The path can be absolute
	or relative. If this option is not specified: on Windows, all Windows
	drive letters will be assigned to PC-BASIC drive letters; on other
	systems, the current working directory is assigned to Z:.

-n
	Run PC-BASIC as a command-line filter. Same as --interface=none.

--output=output_file[:append]
	Send screen output to output_file, except if SCRN: is written to
	explicitly. Output to SCRN: files will always be shown on the screen,
	as in GW-BASIC. If the specifier append is given, the output file is
	appended to rather than overwritten. If output_file is STDIO: or
	STDOUT:, screen output will be sent to standard output.

--peek=[seg:addr:val[,seg:addr:val ... ]]
	Define PEEK preset values. If defined, DEF SEG seg:? PEEK(addr) will
	return val.

--preset=option_block
	Load machine preset options. A preset option corresponds to a section
	defined in a config file by a name between square brackets, like

	[this] --preset=this will load all settings defined in that section.
	Available presets depend on your configuration file. See the list of
	default presets in the User's Guide.

--prevent-close[=True|=False]
	Suppress window close event. This allows BASIC to capture key
	combinations that normally close the window. Graphical interface only.
	By default, the operating system's key combination to close a window
	(usually Alt+F4) terminates PC-BASIC. Set --prevent-close to allow
	BASIC to capture this key combination instead. This is useful if your
	program uses this key combination.

-q --quit[=True|=False]
	Quit interpreter when execution stops. If combined with --run, PC-
	BASIC quits when the program ends. If set in direct mode, PC-BASIC
	quits after the first command is executed.

--reserved-memory=number_of_bytes
	Reserve number_of_bytes of memory at the bottom of the data segment.
	For compatibility with GW-BASIC. Default is 3429 bytes. Lowering this
	value makes more string and variable space available for use by
	programs.

--resume[=True|=False
	Resume from saved state. Overrides --run and --load.

-r=program --run=program
	Run the specified program. Overrides --load.

--scaling={smooth|crisp|native}
	Choose scaling method.

	smooth
		The display is smoothly scaled to the largest size that allows for the
		correct aspect ratio.

	crisp
		The display is scaled to the same size as with smooth, but without
		smoothing.

	native
		Scaling and aspect ratio are optimised for the display's native pixel
		size, without smoothing. --scaling=native overrides --aspect.

	Default is smooth. Only has an effect if combined with
	--interface=graphical.

--serial-buffer-size=size
	Set serial input buffer size. Default is 256. If set to 0, serial
	communications are disabled.

--shell=[shell-executable]
	Enable the SHELL statement to run the operating system command
	interpreter shell-executable. The executable shell-executable should
	support MS-DOS's COMMAND.COM calling conventions, in particular its /C
	switch. Example command interpreters are CMD.EXE on Windows and "wine
	cmd.exe" on Unix. If shell-executable is empty (as it is by default),
	the SHELL statement is disabled.

--soft-linefeed[=True|=False]
	Do not treat LF in text and program files as a line break. This
	enables the highest level of compatibility with GW-BASIC files. If
	this option is set, any Linux or Mac text files need to be converted
	to DOS text before using them with PC-BASIC.

--sound=[none|beep|portaudio|interface]
	Choose the sound engine to use. Not all sound engines will be
	available on all systems.

	none
		Suppress sound output.

	beep
		Use the built-in speaker.

	portaudio
		Use the PortAudio sound generator.

	interface
		Use the native sound engine of the interface, if available.

	Default is interface.

--state=state_file
	Set the save-state file to state_file. Default is pcbasic.session in
	the Application Data directory.

--syntax={advanced|pcjr|tandy}
	Choose BASIC dialect. Available dialects are:

	advanced
		Microsoft GW-BASIC and IBM BASICA

	pcjr
		IBM PCjr Cartridge BASIC

	tandy
		Tandy 1000 GW-BASIC.

	Default is advanced.

-t
	Use text-based interface. Same as --interface=text.

--term=terminal_program
	Set the terminal program run by the PCjr TERM command to
	terminal_program. This only has an effect with --syntax={pcjr|tandy}.

--text-width={40|80}
	Set the number of columns in text mode at startup. Default is 80.

--text-encoding=[encoding]
	Set the text encoding. Text files (i.e. plain-text programs and files
	opened for INPUT and OUTPUT) stored on a disk device will be assumed
	to be encoded in encoding. Examples of valid encodings are utf-8,
	utf-16, latin-1. Please ensure that all characters in the current
	codepage are included in the encoding you choose; if this is not the
	case then such characters will be replaced by � or ?. If encoding is
	not set, text files will be treated as raw bytes in the current PC-
	BASIC codepage.

--utf8[=True|=False]
	Set the text encoding to UTF-8. This option is deprecated and ignored
	if --text-encoding is set. Use --text-encoding instead.

-v --version
	Print PC-BASIC version string and exit.

--video=adapter[:low]
	Set the video adapter to emulate. Available adapters:

	vga
		Video Graphics Array

	ega
		Enhanced Graphics Adapter

	cga
		Color/Graphics Adapter

	cga_old
		Color/Graphics Adapter (old version; affects composite)

	mda
		Monochrome Display Adapter

	hercules
		Hercules Graphics Adapter

	pcjr
		IBM PCjr graphics

	tandy
		Tandy 1000 graphics

	olivetti
		Olivetti M24 graphics

	Default is vga. If the specifier low is given, use low-intensity
	palettes in CGA modes. This only has an effect with adapters
	{cga|cga_old|ega|vga}.

--video-memory=size
	Set the amount of emulated video memory available. This affects the
	number of video pages that can be used. On PCjr and Tandy, this can be
	changed at runtime through the CLEAR statement; at least 32768 needs
	to be available to enter SCREEN 5 and SCREEN 6. Default is 16384 or
	PCjr and Tandy and 262144 on other machine presets.

-w --wait[=True|=False]
	If True, PC-BASIC waits for a keystroke before closing the window on
	exit. Only works for --interface=graphical or --interface=text.
	Default is False.

--options=gwbasic_options
	Set GW-BASIC-style command-line switches. This is a convenience option
	to facilitate migration from GW-BASIC. gwbasic_options is a string
	that may contain the following options:

	/d
		Enable double-precision floating-point math functions. See also
		--double.

	/f:n
		Set the maximum number of open files. See also --max-files.

	/s:n
		Set the maximum record length for RANDOM files. See also --max-reclen.

	/c:n
		Set the size of the receive buffer for COM devices. See also --serial-
		buffer-size.

	/i
		Statically allocate file control blocks and data buffer. Note that
		this is already the default approach in GW-BASIC and PC-BASIC so that
		this option has no effect.

	/m:n,m
		Set the highest memory location to n and maximum BASIC memory size to
		m*16 bytes. See also --max-memory.

	>filename
		Write screen output to filename. See also --output.

	>>filename
		Append screen output to filename. See also --output.

	<filename
		Read keyboard input from filename. See also --input.

	GW-BASIC-style switches are not case sensitive. Note that the symbols
	used in these switches may have different meaning in the shell from
	which PC-BASIC is called; you should quote and escape the options as
	necessary.
