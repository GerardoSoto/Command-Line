usage: pyradio [-h] [--stations STATIONS] [--play [PLAY]] [--add] [--list]

Console radio player

optional arguments:
  -h, --help            show this help message and exit
  --stations STATIONS, -s STATIONS
                        Path on stations csv file.
  --play [PLAY], -p [PLAY]
                        Start and play. The value is num station or empty for
                        random.
  --add, -a             Add station to list.
  --list, -l            List of added stations.
