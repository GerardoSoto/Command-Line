+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+	                QB�s Palette                          +
+                     By James Kinney	                      +
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+	           ~Index~	                              +
+	I: About QB�s Palette                                 +
+	II: What about RGB Values?                            +
+	III: Why Mess with success                            +
+	IV: Cleaning Up	                                      +
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Part: I (About QB�s Palette) QB has a very unique palette�ok I lied. It has a very simple and basic palette, you just have to get to know it. Like almost all palettes, it has a combination of RGB. R meaning red, G meaning green, and B meaning blue. These are the three colors from which all colors come from. Having these colors is a necessity to any palette. The reason behind this is because without those three colors, you couldn�t even have black and white. First, QB�s Default palette. We all know about the first 16 colors, but just to refresh your memory, here they are:

0 - Black
1 � Blue
2 � Green
3 � Cyan
4 � Red
5 � Magenta
6 � Orange
7 � Grey
8 � Dark Grey
9 � Light Blue
10 � Light Green
11 � Light Cyan
12 � Light Red
13 � Light Magenta
14 � Yellow
15 � White
16 � Darkest Grey

There are 256 colors in total. But that is not the point. The point is, all 256 of those colors came from three basic ones, Red, Green, and Blue. Now I�m going to give you some code that you won�t understand at all (that�s just the way I am). Don�t bother trying to understand it, just skim through it then read the explanation below.

OUT &H3C7, colornumber  
R% = INP (&H3C9)  
G% = INP (&H3C9)  
B% = INP (&H3C9)

You may now blink a few times and wipe that confused look off your face. Don�t bother putting this code into QBasic just yet, all this will do is�well, nothing yet. The only thing you need to know is that this code resets the given color (A.K.A. colornumber). This will come in very handy later on. Lets explain it in a little depth. We�ll start with �OUT &H3C7, colornumber�. The OUT command simply accesses a port. This particular port is the Graphics card. The �, colornumber� accesses the color number given from the port. The other three lines of code just reset the RGB values of the given color from the port. Whenever you change the palette, make sure you add this section of code at the end of all your programs. Part II: (What About RGB Values?) As I explained before, RGB stands for Red Green and Blue. And all colors come from this basic combination. But how do they come from this? Remember when you were in 1st or 2nd grade and you learned about mixing paint? That is just like this, Red and Blue make purple, and a certain amount of Blue and Green form Brown. Well how can you do that in QB? Just like you did in 2nd grade, except this time, the amount of paint is a value (1-63) and the colors are colors from your graphics card. Here�s how to do it in code:

OUT &H3C8, COLOR
OUT &H3C9, R%
OUT &H3C9, G%
OUT &H3C9, B% 

Remember this, looks familiar doesn�t it. but its not quite the same, you may notice that some things are in different order and the numbers are different. Let me explain this piece by piece. First, one of the most important parts of the example:

OUT &H3C8, COLOR

This accesses the color from the graphics card. Lets make the color�1. If you refer to the chart above, you notice that 1 is equal to the color blue. What were going to do next is change the RGB values of the color blue. We can than make it almost any color we want. Lets say we want the brightest blue possible. Well, the highest value you can have is 63, so this is how we would do that:

OUT &H3C8, 1     �Color
OUT &H3C9, 1     �R%
OUT &H3C9, 1     �G%
OUT &H3C9, 63    �B%

The first line sets the color to change as Blue. The second line sets the Red value of the color to 1; the Third line sets the Green value. And the last one sets the Blue value. Notice the Red and Green values are as low as possible. And the Blue Value is set as high as possible. This makes the brightest blue. Now, what if we want a brownish color, or hazel. We take the Green value and change it to 63. and leave the Red value at 1. that would create a hazel type color. Now, how about incorporating this newfound knowledge into your programs? Here is a little program that shows a gradient effect:

FOR i = 1 TO 63 STEP 1
	OUT &H3C8, I      �color
	OUT &H3C9, 1      �R%
	OUT &H3C9, 1      �G%
	OUT &H3C9, i      �B%
     LINE (1,i)-(320,i), i
NEXT I

If you read through this, you�ll find out how it works. But just incase you can�t find out how then let me tell you. �i� is the variable in the For-Next Loop. Now, the value goes from 1 to 63. Notice where the variable �i� is placed throughout the program. This loop increments by 1 so everywhere you see �i�, that is whatever number the loop has hit. Now, since this line: �OUT &H3C8, i.� Has the letter �i� in it, that means whatever number the loop has hit, that�s the color it�s changing. And that goes for �OUT &H3C9, i.� As well. You can mess around with this a little if you want, like change the B% to 1 and the G% to �i�. Part III: (Why Mess with success?) Why do you want to change the palette? What if you�re that kind of person who�s afraid of change? Don�t fret; there are many reasons why you would want to change the palette. For instance, what if you make a game (just a thought here) and you want to create a menu system. But you say, �wait, I�ve seen games where the menu pops up, but the background dims itself while the menu is up, how can I do that?�. Or you have a game where you want the room to be dark, but not pitch black. How would you accomplish this? First, you must customize all of your colors. By this, I mean you must make all of your colors using the palette. Let me give you an example:

OUT &H3C8, 1      �BLUE
OUT &H3C9, 1      �R%
OUT &H3C9, 1      �G%
OUT &H3C9, 63     �B%

OUT &H3C8, 2      �GREEN
OUT &H3C9, 1      �R%
OUT &H3C9, 63     �G%
OUT &H3C9, 1      �B%

OUT &H3C8, 3      �RED
OUT &H3C9, 63     �R%
OUT &H3C9, 1      �G%
OUT &H3C9, 1      �B%

This simply, sets colors 1, 2, and 3 to Blue, Green, and Red. So we can put this at the beginning of our program. now that we have the colors set, we need to draw something on the screen, lets make 3 blocks. This is the code that will put three blocks on the screen:

LINE (10,10)-(50,50), 1, BF
LINE (50,10)-(100,50), 2, BF
LINE (100,10)-(150,50), 3, BF

This draws the blocks onto the screen. So now we have 3 Blocks on the screen, a Red Block, a Green block, and a Blue block. Now what say you want the user to press a key to dim it, or make it darker. We need a loop for that. So here�s the code that goes in that loop:

Do UNTIL INKEY$ = CHR$(27)     �Loops until Esc is pressed
 IF INKEY$ = � � THEN          �If the Space bar is pressed, then 
     OUT &H3C8, 1      �BLUE   �it dims the colors�
OUT &H3C9, 1      �R%
OUT &H3C9, 1      �G%
OUT &H3C9, 33     �B%

OUT &H3C8, 2      �GREEN
OUT &H3C9, 1      �R%
OUT &H3C9, 33     �G%
OUT &H3C9, 1      �B%

OUT &H3C8, 3      �RED
OUT &H3C9, 33     �R%
OUT &H3C9, 1      �G%
OUT &H3C9, 1      �B%
 END IF
  LINE (10,10)-(50,50), 1, BF
 LINE (50,10)-(100,50), 2, BF
 LINE (100,10)-(150,50), 3, BF
LOOP

This makes it so if you press the space bar, the colors will dim. So lets put it all together now to make a program that dims the color every time you press the spacebar. Here�s the code:

Clr = 63
Do UNTIL INKEY$ = CHR$(27)     �Loops until Esc is pressed
     OUT &H3C8, 1      �BLUE   
OUT &H3C9, 1      �R%
OUT &H3C9, 1      �G%
OUT &H3C9, clr    �B%

OUT &H3C8, 2      �GREEN
OUT &H3C9, 1      �R%
OUT &H3C9, clr    �G%
OUT &H3C9, 1      �B%

OUT &H3C8, 3      �RED
OUT &H3C9, clr    �R%
OUT &H3C9, 1      �G%
OUT &H3C9, 1      �B%
 IF INKEY$ = � � THEN
 	Clr = clr � 1
 END IF
  LINE (10,10)-(50,50), 1, BF
 LINE (50,10)-(100,50), 2, BF
 LINE (100,10)-(150,50), 3, BF
LOOP

This just dims the colors. If you press the space bar, the variable clr will subtract 1 from itself, thus, making the variable clr worth less. At the beginning of the program, the colors are set as bright as they can be. Every time you press the spacebar, the color gets dimmer and dimmer. Part IV: (Cleaning Up) Add this code to the end of your program:

FOR i = 1 to 256
OUT &H3C7, i  
R% = INP (&H3C9)  
G% = INP (&H3C9)  
B% = INP (&H3C9)
NEXT i

This will Reset all of the colors you changed on the palette. This is very important to put at the end of your program if you want your other programs to run properly. The �INP (&H3C9)� command resets the RGB value given. The loop goes from 1 to 256, because in QB�s Palette, there are 256 colors. So this resets all of the colors in the palette. That�s it for this tutorial, Happy Coding

-Rock Lobster
(A.K.A. Gebmasta)
Special Thanks to P J Haye 