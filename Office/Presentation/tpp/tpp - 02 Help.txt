usage: /usr/bin/tpp [-t <type> -o <file>] <file>
	 -t <type>	set filetype <type> as output format
	 -o <file>	write output to file <file>
	 -s <seconds>	wait <seconds> seconds between slides (with -t autoplay)
	 -x		allow parsing of --exec in input files
	 --version	print the version
	 --help		print this help

	 currently available types: ncurses (default), autoplay, latex, txt
