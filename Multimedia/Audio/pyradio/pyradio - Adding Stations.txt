#############################
# pyradio - Adding Stations #
#############################

The file to edit to add stations (you'll need root access):

    /usr/local/lib/python3.5/dist-packages/pyradio/stations.csv
    
You can test compatibility by using mpv to play the URL.
