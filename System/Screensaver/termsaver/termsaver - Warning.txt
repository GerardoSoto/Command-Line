#######################
# termsaver - Warning #
#######################

It may be tempting to run both 'termsaver' and 'vlock,' but don't. The
only way to stop 'termsaver' is with 'CTRL+c' and 'vlcok' will only show-up
for a split second and then the screensaver continues.
