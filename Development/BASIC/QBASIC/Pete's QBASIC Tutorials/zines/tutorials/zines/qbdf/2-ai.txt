--------------------------
"What Are You Doing Dave?"
--------------------------

	Artifical Intellegence or AI is a MUST for games. 2 player games
are cool and easy to program, as shown in the article "Player too" in the
last issue, but 2 players are not always available to play a game. Thus,
games should be made with the ability to play against AI or anohter player.

	AI can do alot of things. I'm currently working on a Computer
butler name JARVIS, who reacts to incoming data with preset limitations.
More inforamtion about the developement of JARVIS and other aspectsof AI
will be discussed in future issues. This article will focus on creating
AI for games that reacts similar to the way humans do.

	Making a games AI is easy. Giving it strategy, and planning is the
hard part. Also, because of the speed at which a computer runs, AI's can
sometimes be impossible to beat. The following is an example from an old
PACMAN(tm) game I wrote:

MainGameLoop:
'Get input from player
'Compute it
'blah blah blah
'Computers move
IF PacmansX < GhostsX THEN GhostsX = GhostsX - 1
IF PacmansX > GhostsX THEN GhostsX = GhostsX + 1
IF PacmansY < GhostsY THEN GhostsY = GhostsY - 1
IF PacmansY > GhostsY THEN GhostsY = GhostsY + 1
'Do other stuff
GOTO MainGameLoop

	Ok, the above code just basicly says if PACMAN is above the ghost, 
then the ghost moves up, and if Pacamn is below the ghost move down, and if 
he is to the right or left, the move right or left respectfully. These 4 
statements make up the AI of PACMAN. But there is a problem with this. The 
computer gets to evaluate its position in realtion to PACMANs position ever 
time through the main game loop. But a human player has to see the ghost on 
the screen, and move accordingly. All that takes only a faction of a second, 
but in that time, the computer has looped at least 4 times, and the ghost has 
moved 4 more times, while poor PACMAN is waiting to move just once. PACMAN 
can never win against those odds!

	This problem is there because all computers, even OLD computers, don't
run in real-time, that is human time. The US government had this problem in
the mid 70's when it was programming software that connected, at that time,
our nations nuclaer missle silos with radar stations. What they had to do
to stop nuclear holocaust is basicly what you have to do to program PACMAN.
(as amusing as it sounds, its true see "COMPUTER LANGUAGES" by Namomi Baron,
Anchor Books/press, NY NY. 1986) You have to retard the computer so runs at
the speed of a human. Here is an improved version of PACMANs AI

Waitfor = 5
MainGameLoop:
'Get input from player
'Compute it
'blah blah blah
'Computers move
Retard = Retard + 1
IF Retard = Waitfor THEN
IF PacmansX < GhostsX THEN GhostsX = GhostsX - 1
IF PacmansX > GhostsX THEN GhostsX = GhostsX + 1
IF PacmansY < GhostsY THEN GhostsY = GhostsY - 1
IF PacmansY > GhostsY THEN GhostsY = GhostsY + 1
END IF
'Do other stuff
GOTO MainGameLoop

	This is a great method to use because as the player gets father
into the game, the program can always be allowed to move more often by 
lowering the value of Waitfor. On the Acidus version of Pacman, we have:

NextLevel:
Cen40 "Level Clear", 13         
level = level + 1
delayit = delayit +1
if delayit = 3 then Waitfor = Waitfor - 1 :dealyit = 0
Goto reset

	The "delayit" thingy menas that every 3 levels, make the ghosts
faster. This is basicly how PACMAN for the Atari worked, except that was
an arcade game, and was all CMOS chips, and had no software.


--------------------------------------------------------------------------------
This tutorial originally appeared in the QBasic Developers Forum, Issue #2.
This was written by Lord Acidus.