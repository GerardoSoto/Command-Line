*** BINARY NUMBERS, by Alex Warren *******************************************

Binary is a way of representing numbers as a collection of "0"s and
"1"s. Each 0 or 1 in a binary number represents a number. The last digit
represents 1, the second from last 2, the third from last 4, the fourth
from last 8, etc. - doubling each time. This is represented below:

Digit no.       1      2       3      4       5       6       7       8
------------------------------------------------------------------------
Represents    128     64      32      16      8       4       2       1

We use this to turn binary numbers into normal decimal numbers, like
this:

BINARY:         1       0       1       1      0       0      1       0

Where we have a digit '1' above we will add the number it represents to the
decimal number we want to obtain, so the binary number 10110010 above
represents 128+32+16+2 (as these numbers all have "one"s under them), which is
178.

Here are some more examples:

Binary 1010 = Decimal 8+2     = 10
Binary 1111 = Decimal 8+4+2+1 = 15
Binary 1000 = Decimal 8       =  8

If you want it explained another way:
Counting in binary is like counting in decimal, except we can only use
the digits "0" and "1". So the first ten binary numbers are:

BINARY      DECIMAL

0               0
1               1
10              2
11              3
100             4
101             5
110             6
111             7
1000            8
1001            9
1010            10


The program below asks for a binary number and converts it into a
decimal number using the same method as above:

    INPUT "Enter binary number:", num$
    x = (2 ^ LEN(num$)) / 2
    n = 0
    FOR i = 1 TO LEN(num$)
    IF MID$(num$, i, 1) = "1" THEN n = n + x
    x = x / 2
    NEXT i
    PRINT n

Here, num$ is the binary number. x is initialized to the value of the
first binary digit, for example in the case of 101 this will be 8,
and with 10110010 this will be 128. The FOR loop should hopefully be
self-explanatory.

The reverse of this process changes decimal numbers to binary. This
is done easiest using bitwise comparison.

It is done in BASIC using the following:

    IF number AND bindigit THEN .....
    
where the variable "number" is your decimal number and "binval" is your binary
digit, ie 1, 2, 4, 8, 16, 32, 64 etc. So to check whether the decimal 17
includes the binary digit for 2, you would something similar to:

    IF 17 AND 2 THEN PRINT "17 includes 2"


The following program uses the method above to convert any decimal
number into binary.
    

    DIM digit(100) AS STRING
   
    INPUT "Decimal:", decimal

    n = 1: d = 1

    DO
    IF decimal AND n THEN
            digit(d) = "1"
    ELSE
            digit(d) = "0"
    END IF

    n = n * 2
    d = d + 1
    LOOP UNTIL n > decimal
    
    PRINT "Binary: ";

    FOR i = d - 1 TO 1 STEP -1
    PRINT digit(i);
    NEXT i
    
    PRINT


Note how bad this program is, I made it up quickly and it generates the binary
numbers backwards! That's why I have the FOR i = d - 1 to 1 STEP -1 line.



That's it for making binary numbers, but one big question is WHY we would want
to use binary numbers.

Well, if you want to make programs that make use of interrupts, a great
deal of them need binary numbers passed to them. Each digit of the
binary number is usually referred to as a "bit". So, you collect all
your bits together and turn them into a decimal/hexadecimal number,
which you can then pass to your interrupt. (BTW, if you want to convert
your decimal into a hexadecimal number use the BASIC function HEX$)

Binary numbers also have their uses in saving memory and in making
faster, more efficient programs. Here's an example:

If we have conducted a survey of eight questions, each one answered YES
or NO, we could store each person's entire answers in just one single
character - normally, you might save the answers to disk as a list of Y
or N characters, eg YYYNNYNY. If we change Y to 1 and N to 0, we get the
binary number  11100101. We can then convert this to the decimal, which
will be 128+64+32+4+1 = 229. Use CHR$ to turn this into its ASCII
character, and there you have eight answers as one character, taking up
one eighth of the space. If you are saving lots of people's answers to
disk this will save you a LOT of space. We can then extract the original
answers from the number 229 using the method above to get the number
11100101, which you can then convert back to the answers YES, YES, YES,
NO, NO, YES, NO, YES.

You could apply this technique to lots of other things, for example
it can be used in computer games. For example, if you were making an
adventure game, etc., and had a 'Save Game' facility, you could make
each item the player can collect represent a binary digit, eg:

    Coin    Food    Spade   Axe     Bucket      Bowl    Sword
    
If the player has collected the food, bucket and sword you would
convert to binary:

    0       1       0       0       1           0       1
    
So you would convert the binary number 0100101 into decimal and save
as a character. If you have hundreds of items you could split these
up into blocks of eight and save each block as a character.

When reading back your character and converting it to a decimal, you could use
bitwise comparison to check for the spade, for instance. You would use
something similar to:

    items = ASC(itemchar$)
    spadevalue = 16
    IF items AND spadevalue THEN playergotspade = TRUE


Saving binary data into one eighth of the space not only saves on disk space
and memory, it also saves on speed as it is faster to convert for example 100
ASCII characters into the 800 pieces of data they represent than to load eight
times as much data, since hard disks are generally quite slow (this is why
virtual memory under Windows is so slow and more RAM will speed up your
system). This is a much more professional way of saving data.

One important thing to bear in mind when saving data in this way is that you
*MUST* use OPEN filename$ FOR BINARY, and not OPEN filename$ FOR OUTPUT etc.
This is because OPEN filename$ FOR INPUT will not read some chracters
correctly, particularly the null (character 0) and the EOF characaters
(character 26). Saving these values in a text file (which OPEN FOR
INPUT/OUTPUT/etc. is designed for) will often not work correctly.


If you have any more questions then please email dewarr@globalnet.co.uk.









--------------------------------------------------------

*	EDITOR'S NOTE:
*	This article was originally printed in Peter Cooper's BASIX Fanzine, 
*	Issue #9 from October 1997.  This issue was edited by Alex Warren.