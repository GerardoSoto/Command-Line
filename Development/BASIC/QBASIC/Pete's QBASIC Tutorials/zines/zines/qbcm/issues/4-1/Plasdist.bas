'an example of using the distance formula to generate other
'types of Plasmas
'Relsoft 2003

DEFINT A-Z
CLS
SCREEN 13                   '320*200*256
CONST PI = 3.14151693#      ';*)

DIM Lsin1%(-1024 TO 1024)       'Our LookUp tables(LUTS) to speed things up
DIM Lsin2%(-1024 TO 1024)
DIM Lsin3%(-1024 TO 1024)

 FOR i% = -1024 TO 1024         'make sure the LUTS are big enough
    Lsin1%(i%) = SIN(i% * PI / (24)) * 256 - COS(i% * PI / 128) * 132'Precalculate LUTS
    Lsin2%(i%) = SIN(i% * PI / (45)) * 32 + COS(i% * PI / 16) * 128
    Lsin3%(i%) = SIN(i% * PI / (90)) * 64 - COS(i% * PI / 10) * 64
 NEXT i%



'Our nifty palette generator
'wraps the pal around so that the plama would
'look continous.
j! = 255 / 360 * 3          'Maxcolor/2PI*Frequency
k! = 255 / 360 * 1
l! = 255 / 360 * 2
FOR i% = 0 TO 255
    OUT &H3C8, i%
    m% = INT(a!)
    n% = INT(b!)
    o% = INT(C!)
    r% = 63 * ABS(SIN(m% * PI / 180))       'The sine-wwave
    g% = 63 * ABS(SIN(n% * PI / 180))
    b% = 63 * ABS(SIN(o% * PI / 180))
    a! = a! + j!
    b! = b! + k!
    C! = C! + l!
    OUT &H3C9, r%
    OUT &H3C9, g%
    OUT &H3C9, b%
NEXT
DO
FOR y% = 0 TO 100
    FOR x% = 0 TO 100
        dx = ABS(50 - x%)
        dy = ABS(50 - y%)
        dist = SQR(dx * dx + dy * dy)
        C% = Lsin1%(dist + frame%) + Lsin2%(x + frame%) + Lsin3%(y + x)
        PSET (x%, y%), C%
    NEXT x%
NEXT y%
frame% = frame% + 1
LOOP UNTIL INKEY$ <> ""


