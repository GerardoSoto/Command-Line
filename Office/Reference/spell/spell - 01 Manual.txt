SPELL(1)                    General Commands Manual                   SPELL(1)

NAME
       spell - GNU spell, a Unix spell emulator

SYNOPSIS
       spell [options] files ...

DESCRIPTION
       This  manual page documents briefly the spell command. This manual page
       was written for the Debian GNU/Linux distribution (but may be  used  by
       others),  because  the  original  program  does not have a manual page.
       Instead, it has documentation in the GNU Info format; see below.

       spell is a program that emulates the traditional Unix spell command  by
       calling the Ispell utility.

       It  is  a spell checking program which prints each misspelled word on a
       line of its own.

OPTIONS
       The programs follow the  usual  GNU  command  line  syntax,  with  long
       options  starting  with  two  dashes  (`-').  A  summary of options are
       included below. For a complete description, see the Info files.

       -I, --ispell-version
              Print Ispell's version.

       -V, --version
              Print the version number.

       -b, --british
              Use the British dictionary.

       -d, --dictionary=FILE
              Use FILE to look up words.

       -h, --help
              Print a summary of the options.

       -i, --ispell=PROGRAM
              Calls PROGRAM as Ispell.

       -D, --ispell-dictionary=DICTIONARY
              Use the named DICTIONARY to look up words.

       -l, --all-chains
              Ignored; for compatibility.

       -n, --number
              Print line numbers before lines.

       -o, --print-file-name
              Print file names before lines.

       -s, --stop-list=FILE
              Ignored; for compatibility.

       -v, --verbose
              Print words not literally found.

       -x, --print-stems
              Ignored; for compatibility.

SEE ALSO
       The programs are documented  fully  by  GNU  Spell,  a  clone  of  Unix
       `spell', available via the Info system.

AUTHOR
       This manual page was written by Dominik Kubla <dominik@debian.org>, for
       the Debian GNU/Linux system.

                                                                      SPELL(1)
