usage: climage 0.1.3 [-h] [-v] [--unicode | --ascii]
                     [--truecolor | --256color | --16color | --8color]
                     [--palette {default,xterm,linuxconsole,solarized,rxvt,tango,gruvbox,gruvboxdark}]
                     [--quiet] [-w cols] [-o outfile]
                     inputfile

An easy way to convert images for display in terminals

positional arguments:
  inputfile             The image file you wish to convert.

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  --unicode, -u         Sets the output to utilise unicode characters,
                        resulting in a more detailed image. Warning: this is
                        not supported by all terminals.
  --ascii, -a           Restricts the output to ascii characters (default).
  --truecolor, -t       Utilize 16 million colors to encode output, results in
                        more accurate output. Warning: RGB color is not
                        supported by all terminals.
  --256color, -m        Only use 256 colors to encode output (default).
  --16color, -s         Only use 16 colors to encode output.
  --8color, -b          Only use 8 colors to encode output.
  --palette {default,xterm,linuxconsole,solarized,rxvt,tango,gruvbox,gruvboxdark}, -p {default,xterm,linuxconsole,solarized,rxvt,tango,gruvbox,gruvboxdark}
                        Choose a system color palette - only applies to 8, 16,
                        or 256 color modes. This is especially helpful for
                        terminal themes that drastically change the appearance
                        of default collors, achieving more accurate colors on
                        those terminals.
  --quiet, -q           Disable warnings.
  -w cols, --cols cols  Set the number of columns output should contain
                        (default 80).
  -o outfile, --output outfile
                        Choose a file to output to
