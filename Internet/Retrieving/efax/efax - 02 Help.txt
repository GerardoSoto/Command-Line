Usage:
  efax [ option ]... [ -t num [ file... ] ]
Options:
  -a str  use command ATstr to answer
  -c cap  set modem and receive capabilites to cap
  -d dev  use modem on device dev
  -e cmd  exec "/bin/sh -c cmd" for voice calls
  -f fnt  use (PBM) font file fnt for headers
  -g cmd  exec "/bin/sh -c cmd" for data calls
  -h hdr  use page header hdr (use %d's for current page/total pages)
  -i str  send modem command ATstr at start
  -j str  send modem command ATstr after set fax mode
  -k str  send modem command ATstr when done
  -l id   set local identification to id
  -o opt  use protocol option opt:
      0     use class 2.0 instead of class 2 modem commands
      1     use class 1 modem commands
      2     use class 2 modem commands
      a     if first [data mode] answer attempt fails retry as fax
      e     ignore errors in modem initialization commands
      f     use virtual flow control
      h     use hardware flow control
      l     halve lock file polling interval
      n     ignore page retransmission requests
      r     do not reverse received bit order for Class 2 modems
      x     use XON instead of DC2 to trigger reception
      z     add 100 ms to pause before each modem comand (cumulative)
  -q ne   ask for retransmission if more than ne errors per page
  -r pat  save received pages into files pat.001, pat.002, ... 
  -s      share (unlock) modem device while waiting for call
  -v lvl  print messages of type in string lvl (ewinchamr)
  -w      don't answer phone, wait for OK or CONNECT instead
  -x fil  use uucp-style lock file fil
Commands:
  -t      dial num and send fax image files file...
