#############################
# Gentoo - Help Video Links #
#############################

The Gentoo Linux Installation Guide (Antoun Sawires):
----------------------------------------------------
Part 01 - http://www.youtube.com/watch?v=13HUQ0LHI7g
Part 02 - http://www.youtube.com/watch?v=9mD6aAG7DNA
Part 03 - http://www.youtube.com/watch?v=LsD8LqJKfm8

Gentoo Applications & Upgrades(Linux4UnMe):
------------------------------------------
Part 01 - Portage - http://www.youtube.com/watch?v=n0JpIN_kPWk
Part 02 - Portage Upgrades - http://www.youtube.com/watch?v=FFC6315ks5E
Part 03 - Overlays | Eix | Alternative methods - http://www.youtube.com/watch?v=zGBzfc7i3R8
Part 04 - Eselect | Eclean | Equery - http://www.youtube.com/watch?v=M8kkIDcEzLk
Part 05 - Kernel Upgrades - http://www.youtube.com/watch?v=jEaSJ8CzUJA
