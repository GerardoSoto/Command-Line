#################
# vlock - Basic #
#################

If you need to lock your system while you go do whatever like you should
with a screen saver, you can use vlock for TTY/console. On some systems,
you can also use it in a terminal emulator. However, by default, just
running vlock by itself will only lock one TTY/console at a time. If you
want to lock the whole system, you need to use:

    vlock -a
