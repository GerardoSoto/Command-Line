     The QBNews                                                     Page  3
     Volume  1, Number  3                                      May 22, 1990



     ----------------------------------------------------------------------
                         B e g i n n e r s   C o r n e r
     ----------------------------------------------------------------------

     BASIC Menuing and Graphics by Ryan Snodgrass
     
          This  section  of  QB News is dedicated to the  readers  who  are
     novices to Quick BASIC.  In this issue we will talk about creating and
     editing simple menu programs and about some of the elementary commands
     used  in making computerized graphics.  The first thing we talk  about
     is  making simple menus.  The first step you take in making a menu  is
     to plan what selections you will have.  In our example we will use the
     following selections: DIR, make the computer beep, and quit.  The next
     step  is  to  plan whether to use numbers, letters, or  both.  In  our
     example  we  will use both numbers and letters.  The following  is  an
     example program; you may edit it as much as you like.
     
          Menu:
          PRINT "(1) - DIR"    'Shows the first selection (DIR)
          PRINT "(2) - BEEP"    'Shows the second selection (BEEP)
          PRINT "(Q) - QUIT"    'Shows the third selection (QUIT)
         MenuInput:
               A$=INPUT$(1)     'Waits  for the input from  the  keyboard
               IF A$="1" THEN GOTO Selection1   'See if one was pressed
               IF A$="2" THEN GOTO Selection2   'See if two was pressed
               IF A$="Q" OR A$="q" THEN CLS:END    'See if Q was pressed
          GOTO MenuInput    'Go back and wait for another key
     
          Selection1:
               SHELL"DIR"     'Do a DIR of the current  directory
               GOTO Menu    'Redisplay the menu
          Selection2:
               BEEP     'Produce a beep
               GOTO  Menu    'Redisplay the menu
     
     -+* To edit selections on this menu do the following: *+-
     
     1) Change the menu listing by replacing one of the selections by
     another (i.e. PRINT "(1) - DIR" replace with the following:
          PRINT "(1) - DIR/P")
     
     2) Change the selection commands (i.e. change the Selection1  commands
     to:
          Selection2:
               SHELL"DIR/P"
               GOTO Menu
     
     -+*  To add selections to the menu do the following: *+-
     
     1) After the PRINT "(2) - BEEP" add PRINT "(3) - Your Selection"
     
     2) After IF A$="2" THEN... add IF A$="3" THEN GOTO Selection3
     
     3) After the GOTO Menu on Selection2 add:
          Selection3:
     
     The QBNews                                                     Page  4
     Volume  1, Number  3                                      May 22, 1990

               'Your  command
               GOTO  Menu
     
     
          The  next  subject  we will discuss is  elementary  commands  for
     creating  graphics  screens.   A graphics  screen  is  represented  by
     a  number of pixels or points on the screen, which is referred  to  as
     the resolution of your screen.  The first step we must do is select  a
     screen  number  which  corresponds to your  resolution  or  any  other
     resolution you can use.  The following is a list of screen number  and
     their attributes:
     
     +-----------+------------------+-------------+------------------+
     |Screen #:   | # of colors:    | Resolution: | Width of  Screen:|
     +-----------+------------------+-------------+------------------+
     | 1 (CGA)    | 4 out of 16     | 320x200     | 40 Characters    |
     | 2 (CGA)    | 2 out of 2      | 640x200     | 80 Characters    |
     | 7 (EGA)    | 16 out of 64    | 320x200     | 40 Characters    |
     | 8 (EGA)    | 16 out of 64    | 640x200     | 80 Characters    |
     | 9 (EGA)    | 16 out of 64    | 640x350     | 80 Characters    |
     | 10 (MONO)  | 9 grays         | 640x350     | 80 Characters    |
     | 11 (VGA)   | 2 out of 2      | 640x480     | 80 Characters    |
     | 12 (VGA)   | 16 out of 256000| 640x480     | 80 Characters    |
     +-----------+------------------+-------------+------------------+
     
          The  next step is to type: SCREEN (and then the type of  graphics
     you want).  The first and easiest command is LINE.  The usage is  LINE
     (X,Y)-(X,Y),Color  (i.e. LINE (100,100)-(200,200),1 would draw a  line
     from  100,100 to 200,200 using the color 1). Using that in an  example
     program  would be as  follows:
     
          SCREEN  1
          LINE(100,100)-(200,200),1
          END
     
          You  can  also  draw  an open  rectangle  by  using  LINE  (X,Y)-
     (X,Y),Color,B or a solid rectangle by using LINE (X,Y)-(X,Y),Color,BF.
     The  next command we will go into is the DRAW command.  The  usage  is
     DRAW  "(Attributes)" (i.e. DRAW "U4 R4 D4L4").  The attributes are  as
     follows:  C#=Color Number, U#=Up a number of pixels, D#=Down a  number
     of  pixels, L#=Left a number of pixels, R#=Right a number  of  pixels,
     E#=Up and to the right a number of pixels, F#=Down and to the right  a
     number of pixels, G#=Down and left a number of pixels, H#=Up and  left
     a  number  of  pixels, M X,Y=Moves to a certain pixel  (i.e.  DRAW  "M
     100,100"). Our example program draws a 3-D box:
     
          SCREEN  1
          DRAW "M 100,100 C1 U10 R10 D10 L10 H10 U10 F10 R10 H10 L10"
          END
     
          If  you have any suggestions as to what you would like to see  in
     this  column,  please send them to the QBNews at the  address  in  the
     back.