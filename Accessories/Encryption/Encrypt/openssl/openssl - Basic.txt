###################
# openssl - Basic #
###################

Encrypt some text:
-----------------

    echo "Some text" \
    | openssl enc -aes-256-cbc -a -salt -pass pass:password1234

Decrypt some text:
-----------------

    echo "U2FsdGVkX19+Sz8IRLN6WFG/4EIs9LNwBTVBFJpDsIw=" \
    | openssl enc -aes-256-cbc -a -d -salt -pass pass:password1234



Encrypt a folder:
----------------
    tar cz folder_to_encrypt | openssl enc -aes-256-cbc -e > out.tar.gz.enc
compress the folder and send it to openssl for encryption, then use

Decrypt tar.gz
--------------
    openssl aes-256-cbc -d -in out.tar.gz.enc -out decrypted.tar.gz
to get your folder back out again.
