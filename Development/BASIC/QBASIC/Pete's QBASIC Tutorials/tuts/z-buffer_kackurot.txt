			 Z-BUFFER

 	This is information on how to implent a zbuffer in your qbasic games.
Fisrt off, zbuffers are used to draw images/objects in order from least to greatest size. 
The zbuffer algorihm was thought of in the 1970's by E. Catmull. 
The zbuffer is an screen size array filled with object's z coordinate. 
The zbuffer, unlike other sorting algorihms, checks the object  at it's pixel level instead of the coordinate level. 
Zbuffers can be used for 2D and 3D games. You can use it for sorting witch sprite is drawn on the screen for 2d. 
3d, as you know, are used for polly's and other 3d objects. Well.. enough of that, lets see some examples :) .
       image1        image2        images rendered in zbuffer
      __________   ___________    __________
      |000040000|   |100000000|   |100040000|
      |000444000|   |011000000|   |011444000|
      |004444400| + |011110000| = |011114400|  
      |044444440|   |111111100|   |111111140|
      -----------   ----------     ----------

	Now let me explain. Each pixel in the zbuffer has it's on z. So
if image2[x][y] object.z then
        writez object.x,object.y,object.z   
	pixel object.x,object.y
        endif
      endsub

Now before you implent the buffer it must be cleared to zero.
But there's a prob. How will you draw the pixel if the position in the
zbuffer = 0? Well before you draw anything to the buffer you must convert the z like so:

   for i=0 to maxobjects
    object.z = 1/object.z
   next

  example
 object.z = 5
 1/5 = .2

 so when you check the buffer to see if it's geater:
    zbuffer=0
    object.z= .2
    0 > .2
   0 is greater than .2 so draw the pixel
 It's that easy!
 A word of advice is to use ems or xms for the zbuffer and screen buffer. 
Use peek and poke to input valuse's to the zbuffer in xms/ems.
Besides it's faster and saves memory so qb won't start crying.


well thats it, if you don't get this, (cough) send me an e-mail:
Kackurot2002@yahoo.com

Thank yous:
Relsoft,joakim,Fling-master,Blitz, and others who i forgot to mention ;).
bye all! till next time.