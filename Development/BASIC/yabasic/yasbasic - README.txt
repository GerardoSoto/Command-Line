####################
# yabasic - README #
####################

YABASIC stands for Yet Another Basic and it is a BASIC programming language. 
In PAL regions the Playstation 2 was released with a special modified version 
of yabasic (which can be found on the demo disc) for developing games, 
demos and routines. In other words, saving Sony tariff money. However, you
can install YABASIC on Linux, even though it won't do everything it was
technically designed to do. But, I see no reason one couldn't install
'PCSX2' (Playstation 2 emulator), find an ISO of the free demo disc with
YASBIC on it, and try what you've made on your computer.
