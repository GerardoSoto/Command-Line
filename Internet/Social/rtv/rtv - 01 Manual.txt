RTV(1)                                  Usage and Commands                                 RTV(1)

NAME
       RTV - Reddit Terminal Viewer

SYNOPSIS
       rtv [URL] [-s SUBREDDIT]

DESCRIPTION
       RTV (Reddit Terminal Viewer) is a terminal interface to view and interact with reddit.

OPTIONS
       URL    [optional] Full URL of a submission to open

       -h, --help
              show this help message and exit

       -s SUBREDDIT
              Name of the subreddit that will be loaded on start

       --log FILE
              Log HTTP requests to the given file

       --config FILE
              Load configuration settings from the given file

       --ascii
              Enable ascii-only mode

       --monochrome
              Disable color

       --theme FILE
              Color theme to use, see --list-themes for valid options

       --list-themes
              List all of the available color themes

       --non-persistent
              Forget the authenticated user when the program exits

       --clear-auth
              Remove any saved user data before launching

       --copy-config
              Copy the default configuration to {HOME}/.config/rtv/rtv.cfg

       --copy-mailcap
              Copy an example mailcap configuration to {HOME}/.mailcap

       --enable-media
              Open external links using programs defined in the mailcap config

       -V, --version
              show program's version number and exit

       --no-flash
              Disable screen flashing

CONTROLS
       Move the cursor using the arrow keys or vim style movement.
       Press up and down to scroll through submissions.
       Press right to view the selected submission and left to return.
       Press ? to open the help screen.

FILES
       $XDG_CONFIG_HOME/rtv/rtv.cfg
              The configuration file can be used to customize default program settings.

       $XDG_DATA_HOME/rtv/refresh-token
              After  you login to reddit, your most recent OAuth refresh token will be stored for
              future sessions.

       $XDG_DATA_HOME/rtv/history.log
              This file stores URLs that have been recently opened in order to visually highlight
              them as "seen".

ENVIRONMENT
       RTV_EDITOR
              Text editor to use when editing comments and submissions. Will fallback to $EDITOR.

       RTV_URLVIEWER
              Url viewer to use to extract links from comments.  Requires a compatible program to
              be installed.

       RTV_BROWSER
              Web browser to use when opening links. Will fallback to $BROWSER.

AUTHOR
       Michael Lazar <lazar.michael22@gmail.com> (2017).

BUGS
       Report bugs to https://github.com/michael-lazar/rtv/issues

LICENSE
       The MIT License (MIT)

Version 1.22.1                            March 11, 2018                                   RTV(1)
