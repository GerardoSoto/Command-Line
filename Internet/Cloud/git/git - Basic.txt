###############
# git - Basic #
###############

Git's usage seems to confuse a lot of people for some reason. I can kind 
of see why because of the lingo used by hardcore developers, but this is
really all you need to know:

You've found a project on GitHub, GitLab, Bitbucket, etc. and you want to
grab a copy of the project:

    git clone https://gitlab.com/username/projectname.git
    
You want to check for updates for this project:

    cd "/path/to/project"
    git pull
    
You started your own repository on GitLab and now want to start working
on your own project:

    git clone https://gitlab.com/username/projectname.git
    
You've made changes to you project on your computer and want to sync those
changes to GitLab:

    cd "/path/to/project"
    git add .
    git commit -m 'Write something short to summarize what you changed'
    git push
    
    *You may then be prompted for a username and a password
    *If you plan on making a lot of really small changes and don't know
     what to write, just do what I do and use:
     
         git commit -m '...'
         
Oh crap! You made a big mistake and need to roll your project back to a
previous version:

    git log --oneline
    git reset #######
    
    *List the commits made from day 1
    *Reset to commit "#######" in which would look more like '0840a1c,'
     or something like that.
     
     You can also use 'reset' relatively:
     
         git reset -2
         
         *Goes back two commits
