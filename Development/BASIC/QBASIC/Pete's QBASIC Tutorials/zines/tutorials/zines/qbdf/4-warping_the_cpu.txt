---------------
Warping the CPU
---------------

        The PC speaker is an interesting thing. It is great to have. It
Tells us when MS-DOS's keyboard buffer is full, plays noises in Qbasic,
and other neat stuff. There is just one problem, IT IS LOUD AS HELL! There 
are several adjustments you can make with you PC speaker. Now, granted you
COULD go spend $12.99 a piece at Radio Shack (which is owned by Tandy, So I
guess they got out of the crappy computer business, and now to cope with
their anger, harass you by over charging you for small parts),
or you can have alot of fun "modifying" your computer.

        Here are some things you can do with just your speaker (or, as I
have done, all of them)

1-Add an ON/OFF switch
2-Add a volume switch
3-Add a headphone jack
4-Pipe in music
5-Wire it to your stereo

ADD A ON/OFF SWITCH
Get:        
-knife
-hacksaw (optional)
-light switch
-electrical tape
-copper wire


        This is the easiest add-on to your computer. Just go to your local
hardware store, and buy some electrical tape, a light switch and some wire.
Take the hacksaw and saw off the 2 end of the light switch so you are left
with just the switch part. It is about an inch and a half long. Find where
you speaker attaches to your mother board. There should be 2 wires that go
into a little black think that fits over 4 pins (it might be 2 on some
mother boards). The mother board might have "SPKR" or "SPEAKER" printed
under the set of pins, but it with probably say "J-18" or something like 
that. Disconnect the speaker.

        Go about 2 inches up the wire from the black pin holder thingy and
cut ONE of the wires (it doesn't matter which one). Next use some of your
extra wire and connect one end of the cut wire with one of the screws on the
side of the light switch. The wire length depends on where your going to put
the switch. This is where the fun comes in. If you have a tower case. Then
you can drill a hole in one of the unused drive covers and glue the back of
the light switch to the outside. Run the wires through this hole (about the
size of a penny) and connect it). If you have a desktop case, be prepared
for some work. You can either drill a hole in the front of the case, or just
use alot of wire and run them out the back of the computer through one of
the unused expansion slots.

        Ok, so you have one end of the cut wire attached to an extra piece
of wire which runs to wherever you light switch is. Take a second wire,
hook it on to the second screw on the light switch (oh, by the way some
light switches may have 3 screws, one of the is for a ground, but you don't
need one. If you have a light switch with 3 screws, use the 2 screws that are
on the same side) and run it back into the computer and attach it to the
other side of the cut wire. Use electrical tape to secure the places where
the wires join. You may want to use some duct tape and secure the wires
inside the computer to the side of the case, so they don't hang down.
Actually, you better do this if you are thinking of making other "changes"
to your computer. I didn't, and when I tried to wire in music, it was
impossible to get at anything because of this web of wires.

ADD A VOLUME CONTROL
        This is another bigger help. You have you install this instead of
an On/Off switch, but if you turn down the volume, you don't need an
On/Off switch. Just go to a hardware store and buy a dimmer switch. It is a
little knob thing that turns. Attach it to wires in the same way you attached
the light switch. There you go, no more blown out ears!

ADD A HEADPHONE JACK
        Proceed the same way you did with the on/off switch. However, use 2
wires to branch off from the screws on the light switch/ dimmer switch. Go
buy a head phone 1/8 plug headphone extension cord. It should have a 1/8 plug
on each end. Also by an 1/8 male to 1/8 male converter. This is a little
thing about an inch long that has a hole in each end for a 1/8 plug. Cut off
one end of the extension cord. Cut the cord and attach the 2 wires inside to
the 2 wires you attached to the switch. Run the extension cord out the back
of your computer through an expansion slot, and shove the male to male
converter on the end. There, now you have a headphone jack

PIPE SOUND THROW YOUR STEREO
        This is kind of cool, but is a lot of work. Before you do it,
consider if you want it. If you don't have a sound blaster, I would do it.
The only thing is hearing a simple system beep over full surround sound is
damn annoying. Also, if you see the note at the end, you can channel the
sound into your Sound Blaster Speakers, so you don't need the Stereo.

GET:
-a stereo with an AUX settings, that lets you hear music from an outside 
 source
-2 phono plugs
-Copper wire
-1 "Y" 1/8 headphone connectors
-2 1/8 Headphones extension cord (it should have 2  1/8 male plugs, 1 at each
    end)
-1, 1/8 female to female gender changer. This will be about an inch long,
    and have a 1/8 hole at each end

	Ok, if you have an On/off switch/ Volume control already attached to
your speaker, then you will have to remove it. On your motherboard, there
should be a set of 4 pins (I've seen 2 pins, but this is rare) that will
have something like "SPKR" printed on the board under them. There will be a
little black thing about 1/2 inch by 1./2 inch covering these 4(or 2) pins.
There will be 2 wires coming out of this. These 2 wires connect to your
speaker. Unplug the Black thingy and cut both wires about 2 inches away from
the black thingy.

	Next, take your 1/8 plug extension cord, and the plug at one end off
about 10 inches in from the end. There should be 2 wires inside of it. Strip
away the rubber casing around both wires, and cut it, so you have the 2
wires inside of the rubber exposed about 3 inches each. This is tricky,
because you might cut the wires inside when trying to remove the rubber.

	Now ,take the 2 wires that were inside the rubber extension cord,
and attach each one to one of the wires on the black thingy that was on the
mother board. You should end up with this black thingy with 2 wires attaches
to an extension cord at one end. The cord will run about 6-8 feet and end in
a 1/8 male plug. From this step, you can pipe in the music through your
stereo, or your Sound Blaster speakers. To attack to a sound card. Plug it 
into the Music IN hole.

THE FOLLOWING IS HOW TO ATTACH TO A STEREO:	

Take the other extension cord. Cut off 1 end. Attach the phono plugs to the
wires. YOU NEED TO TAKE EACH WIRE COMING OUT OF THE CORD ADN ATTACH 2 WIRES
TO IT. This way you have 4 wires, 2 wires coming from each of the wires in
the cord. Pick 2 of the wires attach to 1 of the original wires to be
negative, and the other2 as positive. No attach 1 positive and one negative
to each plug. This will give you stereo sound instead of mono. Plug them
into the AUX IN phono plugs on the back of your stereo. use a male to male
converter to attach the 2 extension cords.

PIPE IN MUSIC TO PLAY

Use the exact same setup as above, but attach a Y-adapter to the end of the
male to male converter on the end that you plugged in the extension cord that
is attached to the motherboard. Use another extension cord that is male/male
and plug one end into a Walkman, CD player, etc and then the other into the
open hole in the Y-adapter


--------------------------------------------------------------------------------
This tutorial originally appeared in the QBasic Developers Forum, Issue #4.
This was written by Lord Acidus.