##############
# ar - Basic #
##############

Create an archive from multiple text files:
------------------------------------------

    ar r example.a *.txt
    
List contents of archive (.a):
-----------------------------

    ar t example.a
    
Display contents of every file inside of an archive (.a):
--------------------------------------------------------

    ar p example.a
    
Add another file to already made archive (.a):
---------------------------------------------

    ar r example.a AnotherFile.txt
    
Delete a file from an archive (.a):
----------------------------------

    ar d example.a FileToDelete.txt
