'/////Supporting code for the article at QBCM.Hybd.Net

'Plasmas explained!!!
'This is not optimized code.
'The purpose of this snippet is to show you how to use
'the SIN function to generate a non-static non
'palette rotated plasma. You could use the cosine function or
'a combination of both to have some nifty fx.
'notes on optimization:
'1. Direct VGA poke
'2. Buffered Scanline poke
'3. Y- function should not be on the X-loop. ;*)

'Relsoft 2003
'rel.betterwebbercom

DEFINT A-Z
CLS
SCREEN 13                   '320*200*256
CONST PI = 3.14151693#      ';*)

DIM Lsin1%(-1024 TO 1024)       'Our LookUp tables(LUTS) to speed things up
DIM Lsin2%(-1024 TO 1024)
DIM Lsin3%(-1024 TO 1024)

 FOR i% = -1024 TO 1024         'make sure the LUTS are big enough
    Lsin1%(i%) = SIN(i% * PI / (270)) * 256     'Precalculate LUTS
    Lsin2%(i%) = SIN(i% * PI / (45)) * 32
    Lsin3%(i%) = SIN(i% * PI / (90)) * 64
 NEXT i%


'Our nifty palette generator
'wraps the pal around so that the plama would
'look continous.
j! = 255 / 360 * 3          'Maxcolor/2PI*Frequency
k! = 255 / 360 * 2
l! = 255 / 360 * 1
FOR i% = 0 TO 255
    OUT &H3C8, i%
    m% = INT(a!)
    n% = INT(b!)
    o% = INT(c!)
    r% = 63 * ABS(SIN(m% * PI / 180))       'The sine-wwave
    g% = 63 * ABS(SIN(n% * PI / 180))
    b% = 63 * ABS(SIN(o% * PI / 180))
    a! = a! + j!
    b! = b! + k!
    c! = c! + l!
    OUT &H3C9, r%
    OUT &H3C9, g%
    OUT &H3C9, b%
NEXT


'Draw the pal
FOR i = 0 TO 255
    LINE (i, 100)-(i, 150), i
NEXT i

LOCATE 1, 1
PRINT "This is our Palette."
PRINT "It's important to have a pal that"
PRINT "Wraps-around itself."
PRINT "See how color 255 wraps to color 0? ;*)"
PRINT "Press any Key..."

c$ = INPUT$(1)

'Function 1
FOR y% = 0 TO 199
    FOR x% = 0 TO 319
        c% = Lsin1%(x%)
        PSET (x%, y%), c%
    NEXT x%
NEXT y%
LOCATE 1, 1
PRINT "This is our 1st sin function"
PRINT "C% = SIN(x*PI / (270)) * 256"
PRINT "Press any Key..."
c$ = INPUT$(1)

'Function 2
FOR y% = 0 TO 199
    FOR x% = 0 TO 319
        c% = Lsin2%(y%)
        PSET (x%, y%), c%
    NEXT x%
NEXT y%
LOCATE 1, 1
PRINT "This is our 2nd sin function"
PRINT "C% = SIN(y*PI / (45)) * 32"
PRINT "Press any Key..."
PRINT
PRINT "Let's add the 2 fuctions together..."
c$ = INPUT$(1)

'First addition
FOR y% = 0 TO 199
    FOR x% = 0 TO 319
        c% = Lsin1%(x%) + Lsin2%(y%)
        PSET (x%, y%), c%
    NEXT x%
NEXT y%
LOCATE 1, 1
PRINT "This is what you get when you add"
PRINT "the previous two functions together."
PRINT "Press any key..."
c$ = INPUT$(1)


'Function 3
FOR y% = 0 TO 199
    FOR x% = 0 TO 319
        c% = Lsin3%(x% + y)
        PSET (x%, y%), c%
    NEXT x%
NEXT y%
LOCATE 1, 1
PRINT "This is our 3rd sin function"
PRINT "C%=SIN((x+y)*PI / (90)) * 64"
PRINT
PRINT "Let's add this last function"
PRINT "to our previous added functions..."
PRINT "Press any Key..."
c$ = INPUT$(1)


'Second addition
FOR y% = 0 TO 199
    FOR x% = 0 TO 319
        c% = Lsin1%(x%) + Lsin2%(y%) + Lsin3%(y% + x%)
        PSET (x%, y%), c%
    NEXT x%
NEXT y%
LOCATE 1, 1
PRINT "This is what you get when you add"
PRINT "the previous three functions together."
PRINT "Looking good eh?"

PRINT
PRINT "Looks kinda boring when its not moving."
PRINT "Let's add a frame counter to change"
PRINT "the plasma's state."
PRINT "Press any key..."
c$ = INPUT$(1)

'State 1
Counter% = 10
FOR y% = 0 TO 199
    FOR x% = 0 TO 319
        c% = Lsin1%(x%) + Lsin2%(y%) + Lsin3%(y% + x% + Counter%)
        PSET (x%, y%), c%
    NEXT x%
NEXT y%
LOCATE 1, 1
PRINT "State 1 : Frame Counter=10"
PRINT "Press any key..."
c$ = INPUT$(1)

'State 2
Counter% = 50
FOR y% = 0 TO 199
    FOR x% = 0 TO 319
        c% = Lsin1%(x%) + Lsin2%(y%) + Lsin3%(y% + x% + Counter%)
        PSET (x%, y%), c%
    NEXT x%
NEXT y%
LOCATE 1, 1
PRINT "State 2 Frame Counter=50"

PRINT
PRINT "Let's increment the frame counter by"
PRINT "one in every frame to move"
PRINT "Press any key..."
c$ = INPUT$(1)

Dir% = 1
DO
    Counter& = (Counter& + Dir%)
    IF Counter& > 500 THEN Dir% = -Dir%
    IF Counter& < -600 THEN Dir% = -Dir%
    FOR y% = 0 TO 199
        FOR x% = 0 TO 319
            c% = Lsin1%(x%) + Lsin2%(y%) + Lsin3%(y% + x% + Counter&)
            PSET (x%, y%), c%
        NEXT x%
    NEXT y%
LOOP UNTIL INKEY$ <> ""

END


