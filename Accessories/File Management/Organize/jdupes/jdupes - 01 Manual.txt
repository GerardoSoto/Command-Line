FDUPES(1)                   General Commands Manual                  FDUPES(1)

NAME
       jdupes - finds and performs actions upon duplicate files

SYNOPSIS
       jdupes [ options ] DIRECTORY ...

DESCRIPTION
       Searches the given path(s) for duplicate files. Such files are found by
       comparing file sizes, then partial and full file hashes, followed by  a
       byte-by-byte comparison.

OPTIONS
       -A --nohidden
              exclude hidden files from consideration

       -B --dedupe
              issue the btrfs same-extents ioctl to trigger a deduplication on
              disk. The program must be built  with  btrfs  support  for  this
              option to be available

       -D --debug
              if  this  feature  is compiled in, show debugging statistics and
              info at the end of program execution

       -d --delete
              prompt user for files to  preserve,  deleting  all  others  (see
              CAVEATS below)

       -f --omitfirst
              omit the first file in each set of matches

       -H --hardlinks
              normally,  when  two  or  more files point to the same disk area
              they are treated as non-duplicates; this option will change this
              behavior

       -h --help
              displays help

       -i --reverse
              reverse (invert) the sort order of matches

       -I --isolate
              isolate each command-line parameter from one another; only match
              if the files are under different parameter specifications

       -L --hardlink
              replace all duplicate files with hardlinks to the first file  in
              each set of duplicates

       -m --summarize
              summarize duplicate files information

       -N --noprompt
              when  used  together  with  --delete, preserve the first file in
              each set of duplicates and delete the others  without  prompting
              the user

       -n --noempty
              exclude zero-length files from consideration; this option is the
              default behavior and does nothing (also see -z/--zeromatch)

       -O --paramorder
              parameter order preservation is more important than  the  chosen
              sort;  this  is particularly useful with the -N option to ensure
              that automatic deletion behaves in a controllable way

       -o --order=WORD
              order files according to WORD: time - sort by modification  time
              name - sort by filename (default)

       -p --permissions
              don't  consider  files  with different owner/group or permission
              bits as duplicates

       -Q --quick
              [WARNING: RISK OF DATA LOSS,  SEE  CAVEATS]  skip  byte-for-byte
              verification of duplicate pairs (use hashes only)

       -q --quiet
              hide progress indicator

       -R --recurse:
              for each directory given after this option follow subdirectories
              encountered within (note the ':' at the end of option;  see  the
              Examples section below for further explanation)

       -r --recurse
              for  every  directory  given  follow  subdirectories encountered
              within

       -s --linksoft
              replace all duplicate files with symlinks to the first  file  in
              each set of duplicates

       -S --size
              show size of duplicate files

       -s --symlinks
              follow symlinked directories

       -v --version
              display jdupes version and compilation feature flags

       -x --xsize=[+]SIZE
              exclude  files  of size less than SIZE from consideration, or if
              SIZE is prefixed with a '+' i.e.  jdupes -x  +226  [files]  then
              exclude  files  larger  than SIZE. The following suffixes can be
              used and are not case-sensitive:

              `K'    for kilobytes (units of 1024 bytes)

              `M'    for megabytes (units of 1024 x 1024 bytes)

              `G'    for gigabytes (units of 1024 x 1024 x 1024 bytes)

       -z --zeromatch
              consider zero-length files to be duplicates; this  replaces  the
              old default behavior when -n was not specified

       -Z --softabort
              if  the  user  aborts  the  program  (as with CTRL-C) act on the
              matches that were found before the abort was received. For exam‐
              ple,  if -L and -Z are specified, all matches found prior to the
              abort will be hard linked. The default behavior without -Z is to
              abort without taking any actions.

NOTES
       A  set of arrows are used in hard linking to show what action was taken
       on each link candidate. These arrows are as follows:

       ---->  This file was successfully hard linked to the first file in  the
              duplicate chain

       -@@->  This  file  was  successfully symlinked to the first file in the
              chain

       -==->  This file was already a hard link to the first file in the chain

       -//->  Linking this file failed due to  an  error  during  the  linking
              process

       Duplicate  files are listed together in groups with each file displayed
       on a separate line. The groups are then separated from  each  other  by
       blank lines.

EXAMPLES
       jdupes a --recurse: b
              will follow subdirectories under b, but not those under a.

       jdupes a --recurse b
              will follow subdirectories under both a and b.

       jdupes -O dir1 dir3 dir2
              will  always  place 'dir1' results first in any match set (where
              relevant)

CAVEATS
       When using -d or --delete, care should be taken to insure against acci‐
       dental data loss.

       -Z  or  --softabort  used to be --hardabort in jdupes prior to v1.5 and
       had the opposite behavior.  Defaulting to taking  action  on  abort  is
       probably  not  what  most  users  would  expect. The decision to invert
       rather than reassign to a different switch was made because  this  fea‐
       ture was still fairly new at the time of the change.

       The -O or --paramorder option allows the user greater control over what
       appears in the first position of a match set, specifically for  keeping
       the  -N  option  from deleting all but one file in a set in a seemingly
       random way. All directories specified on the command line will be  used
       as  the  sorting  order  of  result sets first, followed by the sorting
       algorithm set by the -o or --order option. This means that the order of
       all  match  pairs  for a single directory specification will retain the
       old sorting behavior even if this option is specified.

       When used together with options -s or --symlink, a user could  acciden‐
       tally preserve a symlink while deleting the file it points to.

       Furthermore, when specifying a particular directory more than once, all
       files within that directory will be listed  as  their  own  duplicates,
       leading  to data loss should a user preserve a file without its "dupli‐
       cate" (the file itself!).

       The -Q or --quick option only reads each file once, hashes it, and per‐
       forms comparisons based solely on the hashes. There is a small but sig‐
       nificant risk of a hash collision which is the purpose of the  failsafe
       byte-for-byte  comparison  that this option explicitly bypasses. Do not
       use it on ANY data set for which any amount of data loss  is  unaccept‐
       able.  This option is not included in the help text for the program due
       to its risky nature.  You have been warned!

REPORTING BUGS
       Send all bug reports to jody@jodybruchon.com or use the  Issue  tracker
       at http://github.com/jbruchon/jdupes/issues

AUTHOR
       jdupes  is a fork of 'fdupes' which is maintained by and contains extra
       code copyrighted by Jody Bruchon <jody@jodybruchon.com>

       jdupes was once called 'fdupes-jody' but the name was  changed  at  the
       request of Adrian Lopez to avoid confusion between the two programs.

       Based on 'fdupes' created by Adrian Lopez <adrian2@caribe.net>

                                                                     FDUPES(1)
