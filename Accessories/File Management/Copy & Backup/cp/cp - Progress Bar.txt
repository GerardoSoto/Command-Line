##############
# cp - Basic #
##############

Copy a file:

    cp "/path/to/file" "/path/to/destination/"
    
Copy a folder:

    cp -R "/path/to/folder" "/path/to/destination/"
    
Copy a file and rename at the same time:

    cp "/path/to/fileA" "/path/to/destination/fileB"
    
Copy with a progress bar:

    cp "/path/to/file" "/path/to/destination/" | pv
    
    *pv is a package that may or may not be included by default on your
     system
