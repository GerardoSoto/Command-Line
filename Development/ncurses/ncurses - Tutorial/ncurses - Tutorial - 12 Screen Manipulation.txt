13. Screen Manipulation

In this section, we will look into some functions, which allow us to 
manage the screen efficiently and to write some fancy programs. This is 
especially important in writing games.
13.1. getyx() functions

The function getyx() can be used to find out the present cursor co-ordinates. 
It will fill the values of x and y co-ordinates in the arguments given to 
it. Since getyx() is a macro you don't have to pass the address of the 
variables. It can be called as

    getyx(win, y, x);
    /* win: window pointer
     *   y, x: y, x co-ordinates will be put into this variables 
     */

The function getparyx() gets the beginning co-ordinates of the sub window 
relative to the main window. This is some times useful to update a sub 
window. When designing fancy stuff like writing multiple menus, it becomes 
difficult to store the menu positions, their first option co-ordinates etc. 
A simple solution to this problem, is to create menus in sub windows and 
later find the starting co-ordinates of the menus by using getparyx().

The functions getbegyx() and getmaxyx() store current window's beginning 
and maximum co-ordinates. These functions are useful in the same way as 
above in managing the windows and sub windows effectively.

13.2. Screen Dumping

While writing games, some times it becomes necessary to store the state 
of the screen and restore it back to the same state. The function scr_dump() 
can be used to dump the screen contents to a file given as an argument. 
Later it can be restored by scr_restore function. These two simple functions 
can be used effectively to maintain a fast moving game with changing scenarios.

13.3. Window Dumping

To store and restore windows, the functions putwin() and getwin() can be 
used. putwin() puts the present window state into a file, which can be 
later restored by getwin().

The function copywin() can be used to copy a window completely onto another 
window. It takes the source and destination windows as parameters and 
according to the rectangle specified, it copies the rectangular region 
from source to destination window. It's last parameter specifies whether 
to overwrite or just overlay the contents on to the destination window. 
If this argument is true, then the copying is non-destructive.
