You can redirect a page to either a modern version or an older version of an HTML file based on whether of not it supports JavaScript. This works in most instances except text-based web browsers. So, that means you must place the following in the head of the HTML file as well as supply the rest of the older version of your HTML just in case the browser wont redirect:

      <noscript>
		<meta http-equiv="refresh" content="0; url=./OlderBrowser/StreamPi.html" />
		<p>If you are seeing this, that means your browser will not refresh based on whether or not it supports JavaScript.</p>
		<p>Are you using a text-browser?...Cool beans :)</p>
	  </noscript>
