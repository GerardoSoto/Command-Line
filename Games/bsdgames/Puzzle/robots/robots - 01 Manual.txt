ROBOTS(6)                                BSD Games Manual                               ROBOTS(6)

NAME
     robots — fight off villainous robots

SYNOPSIS
     robots [-Asjtan] [scorefile]

DESCRIPTION
     robots pits you against evil robots, who are trying to kill you (which is why they are
     evil).  Fortunately for you, even though they are evil, they are not very bright and have a
     habit of bumping into each other, thus destroying themselves.  In order to survive, you must
     get them to kill each other off, since you have no offensive weaponry.

     Since you are stuck without offensive weaponry, you are endowed with one piece of defensive
     weaponry: a teleportation device.  When two robots run into each other or a junk pile, they
     die.  If a robot runs into you, you die.  When a robot dies, you get 10 points, and when all
     the robots die, you start on the next field.  This keeps up until they finally get you.

     Robots are represented on the screen by a ‘+’, the junk heaps from their collisions by a
     ‘∗’, and you (the good guy) by a ‘@’.

     The commands are:
     h       move one square left
     l       move one square right
     k       move one square up
     j       move one square down
     y       move one square up and left
     u       move one square up and right
     b       move one square down and left
     n       move one square down and right
     .       (also space) do nothing for one turn
     HJKLBNYU
             run as far as possible in the given direction
     >       do nothing for as long as possible
     t       teleport to a random location
     w       wait until you die or they all do
     q       quit
     ^L      redraw the screen

     All commands can be preceded by a count.

     If you use the ‘w’ command and survive to the next level, you will get a bonus of 10% for
     each robot which died after you decided to wait.  If you die, however, you get nothing.  For
     all other commands, the program will save you from typos by stopping short of being eaten.
     However, with ‘w’ you take the risk of dying by miscalculation.

     Only five scores are allowed per user on the score file.  If you make it into the score
     file, you will be shown the list at the end of the game.  If an alternative score file is
     specified, that will be used instead of the standard file for scores.

     The options are

     -s      Don't play, just show the score file.

     -j      Jump, i.e., when you run, don't show any intermediate positions; only show things at
             the end.  This is useful on slow terminals.

     -t      Teleport automatically when you have no other option.  This is a little disconcert‐
             ing until you get used to it, and then it is very nice.

     -a      Advance into the higher levels directly, skipping the lower, easier levels.

     -A      Auto-bot mode.  Lets the game play itself.

     -n      Increase the number of games played by one.

AUTHOR
     Ken Arnold Christos Zoulas     (autobot mode)

FILES
     /var/games/bsdgames/robots_roll  the score file

BUGS
     Bugs?  You crazy, man?!?

BSD                                        May 31, 1993                                       BSD
