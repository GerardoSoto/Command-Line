###########################
# when - Example Calendar #
###########################

When you run 'when' for the first time, a file called 'calendar' is
created inside of a ~/.when/ folder. Add the following to have common
holidays:

m=jan & w=mon & a=3 , Martin Luther King Day
* feb 14 , Valentine's Day
m=feb & w=mon & a=3 , Washington's Birthday observed
m=may & w=sun & a=2 , Mother's Day
m=may & w=mon & b=1 , Memorial Day
m=jun & w=sun & a=3 , Father's Day
* jul 4 , Independence Day
m=sep & w=mon & a=1 , Labor Day
m=oct & w=mon & a=2 , Columbus Day
m=oct & w=mon & a=2 , Thanksgiving (Canada)
* nov 11 , Armistice Day
m=nov & w=thu & a=4 , Thanksgiving (U.S.)
e=47 , Mardi Gras
e=46 , Ash Wednesday
e=7 , Palm Sunday
e=0 , Easter Sunday
e=0-49 , Pentecost (49 days after easter)

And now when you type 'when' in the terminal, the closest event or holiday
within a two-week period should show up along with the current date and
time. This is probably the most "UNIXy," basic calendar/scheduling program 
you are going to find for Linux/UNIX systems.
