TREK(6)                                  BSD Games Manual                                 TREK(6)

NAME
     trek — trekkie game

SYNOPSIS
     trek [[-a] file]

DESCRIPTION
     trek is a game of space glory and war.  Below is a summary of commands.  For complete docu‐
     mentation, see Trek by Eric Allman.

     If a filename is given, a log of the game is written onto that file.  If the -a flag is
     given before the filename, that file is appended to, not truncated.

     The game will ask you what length game you would like.  Valid responses are “short”,
     “medium”, and “long”.  You may also type “restart”, which restarts a previously saved game.
     You will then be prompted for the skill, to which you must respond “novice”, “fair”, “good”,
     “expert”, “commodore”, or “impossible”.  You should normally start out with a novice and
     work up.

     In general, throughout the game, if you forget what is appropriate the game will tell you
     what it expects if you just type in a question mark.

AUTHOR
     Eric Allman

SEE ALSO
     /usr/share/doc/bsdgames/trek.me

COMMAND SUMMARY
     abandon
     capture
     cloak up/down
     computer request; ...
     damages
     destruct
     dock
     help
     impulse course distance
     lrscan
     move course distance
     phasers automatic amount
     phasers manual amt1 course1 spread1 ...
     torpedo course [yes] angle/no
     ram course distance
     rest time
     shell
     shields up/down
     srscan [yes/ no]
     status
     terminate yes/no
     undock
     visual course
     warp warp_factor

BSD                                     December 30, 1993                                     BSD
