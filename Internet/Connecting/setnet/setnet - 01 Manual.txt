SETNET(8)                   System Manager's Manual                  SETNET(8)

NAME
     setnet.sh — minimalist shell script for network configuration with dialog
     interface

SYNOPSIS
     setnet.sh [-c config_file] [-d trace_file] [-h] [-v]

DESCRIPTION
     setnet.sh is a shell script for network management, with a terminal user
     interface based on dialog(1). It works as a wrapper around the basic
     tools for the configuration of Ethernet and Wi-Fi interfaces, including
     ip(8), dhclient(8), and wpa_cli(8).

     setnet.sh allows to configure static and/or DHCP-based connections to a
     LAN/WLAN, and to manage the authentication to a Wi-Fi network. At
     present, open (no key), WPA-Personal (WPA-PSK and WPA2-PSK), and WPA-
     Enterprise (EAP/PEAP or EAP/TLS) are supported.

     setnet.sh can be also used as a minimal interface to basic network diag‐
     nostics tools, including ping(8), host(1), traceroute(1), and netstat(8).
     It allows to show some information about network status, including the
     routing table, content of resolv.conf and nsswitch.conf, active TCP/IP
     connections, running daemons, etc., and to dump those information to a
     file. Support for posting a dump of network status to websites like
     pastebin.com is under development.

     setnet.sh uses /bin/sh by default. It has been tested with several stan‐
     dard shells, including bash, busybox, dash, ksh, mksh, posh, sh, yash,
     and zsh but it should work correctly with any POSIX-compliant shell.

     setnet.sh recognises the following options:

     -c config_file
             specify a configuration file

     -d trace_file
             enable dialog debug, and write the trace to the provided
             trace_file

     -h      print a minimal help and exit

     -v      show version and exit

     The main setnet.sh menu allows to choose from five sections, as described
     below:

     Setup   This section allows to configure a network interface, selected
             from the list of available network devices (as returned by "ip -o
             link"). Once a device has been selected, it is possible to choose
             on of the following functions

             View   View current device configuration, i.e., MAC address, link
                    status, IP address, etc.

             WiFi   (available only for wi-fi devices) Manage the association
                    to a Wi-Fi network. This allows to restart wpa_supplicant,
                    to manage the currently configured networks, to add a new
                    network, and to show (and modify) the configuration file
                    used by wpa_supplicant.

             Conf   Configure the IP address of the device. It is possible to
                    choose between DHCP-based and Static IP configuration.

             Start  Bring the interface up (using "ip link set <DEVNAME> up").

             Stop   Bring the interface down (using "ip link set <DEVNAME>
                    down").

             Restart
                    Restart the interface by putting it first down and then up
                    again.

     Info    This section provides information about the current network sta‐
             tus and allows to perform basic diagnostic tasks. The following
             functions are available:

             ARP   Show the current ARP table

             Connections
                   List active network connections by running "netstat -tnp"

             DNS   List the configured DNS, by showing the "nameserver"
                   entries in /etc/resolv.conf.

             Lookup
                   Perform a DNS lookup through "host <HOST>". If <HOST> is a
                   FQDN, the result will be the IP address(es) associated to
                   that domain name. If <HOST> is an IP address, the result is
                   the list of FQDNs associated to that address.

             Ping  Ping a host, using its IP of FQDN (Fully-Qualified Domain
                   Name) by running "ping -c 5 <HOST>".

             Resolver
                   Show the system resolver configuration, i.e. the content of
                   the file /etc/nsswitch.conf

             Routes
                   Show the current routing table

             Services
                   Show a list of processes (daemons) listening on TCP ports,
                   by running "netstat -ltnp".

             Traceroute
                   Show the route to a given host, as obtained by running the
                   command "traceroute <HOST>".

     Dump    Dump information about current network status to a file. The user
             can choose which information to include from a checklist. The
             support for dumping network information to web applications like
             pastebin is currently under development.

     Log     Show the logfile written by setnet.sh

     About   Provide information about copyright, contributors, and license.

ENVIRONMENT
     setnet.sh uses the following environment variables:

     WPA_FILE
             The configuration file for wpa_supplicant(1) (default to
             /etc/wpa_supplicant/wpa_setnet.conf ).

     LOGFILE
             The file used by setnet.sh for logging  (default to
             /var/log/setnet.log ).

     TRUNCATE_LOG
             If it is set to "yes" or "YES", the log file is truncated when
             setnet.sh starts.

     WIFI_DEVICES
             List of wifi devices. This list is used only when automatic
             detection of wifi devices fails.

     SUPPRESS_INFO
             If set to "yes" or "YES", setnet will suppress verbose notifica‐
             tions.

     SHOW_LO
             If set to "yes" or "YES" setnet will include the loopback device
             in the list of available network devices.

     These variables are normally set in the setnetrc configuration file (see
     FILES below). setnet.sh will exit if either WPA_FILE or LOGFILE are unde‐
     fined.

FILES
     setnet.sh uses a configuration file which defines the environment vari‐
     ables described in ENVIRONMENT above. setnet.sh looks for the following
     files:

           ·   the config_file provided through the option -c (if any).

           ·   the file /etc/setnetrc (if it exists)

           ·   the file ~/.setnetrc (if it exists)

     in that order. If a config_file is provided, the other files are ignored.
     Otherwise, if ~/.setnetrc exists, it takes precedence over /etc/setnetrc.

SEE ALSO
     ip(8), iw(8), dhclient(8), wpa_supplicant(8), wpa_cli(8), netstat(8).

AUTHORS
     setnet.sh is Free Software, distributed under the terms of the GNU Gen‐
     eral Public License (GPL), version 3 of the License.

     setnet.sh is copyleft (c) 2016-2018 by Vincenzo (KatolaZ) Nicosia
     <katolaz@freaknet.org>

BUGS
     At present, setnet.sh can reliably manage no more than one Wi-Fi device
     at a time. This is due to the fact that the configuration file for
     wpa_supplicant is defined in the environment variable WPA_FILE. There is
     no limitation on the number of Ethernet devices that setnet.sh can man‐
     age, but you should be careful with having more than one dhclient pro‐
     cesses running, since each of them will try to add a default gateway to
     the kernel routing table.

SECURITY CONSIDERATIONS
     Configuring networking is an administration task, and setnet.sh requires
     root privileges to work properly. It is possible to allow a regular user
     to run setnet.sh, e.g. by using sudo(8) or sup(1) (see
     https://sup.dyne.org/ for more information about sup).

     setnet.sh can recognise if it is running under sudo(8) or sup(1), by
     looking at the content of the environment variables SUDO_UID/SUP_UID,
     SUDO_GID/SUP_GID, and SUDO_USER/SUP_USER. When setnet.sh is run under
     sudo(8) or sup(1), some functionalities are disabled. In particular,
     loading and editing an alternate wpa_supplicant configuration file is
     forbidden (and for obvious reasons, since this would in principle allow
     the sudoer to edit *any* file in your system).

Linux                            May 03, 2018                            Linux
