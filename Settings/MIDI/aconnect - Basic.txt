####################
# aconnect - Basic #
####################

The 'aconnect' command is the quickist and easiest way I know of to get
a MIDI keyboard or device working with programs that will run without
JACK, programs like zynaddsubfx, yoshimi, hydrogen, etc.

List programs and devices:

    aconnect --list
    
Now take the client number beside the midi keyboard and then the client 
number beside the program and use as:

    aconnect <client # of device> <client # of program>
    
They will remain connected until you either close the program, unplug the
MIDI device, or rerun the above.
