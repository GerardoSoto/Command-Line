#######################
# python - Basic Math #
#######################

Using Python for math is much easier than trying to use something like
'bc' or 'concalc.' All you have to do is run 'python' in a terminal and
then use examples:

    >>>3+4
    7
    
    >>>10-2
    8
    
    >>>20*3
    60
    
    >>>13/2
    6
    
    ?! Wait... What ?!
    This is because when in doubt, use decimals such as:
    
    >>>13.0/2
    6.5

A huge advantge of using python is easy use of variables such as:

    >>>Dog = 3
    >>>Cat = 2
    >>>Cat + Dog
    5

When you are done Python, run 'exit()' to quit.
