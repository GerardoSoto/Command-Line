aewan(1)                             General Commands Manual                             aewan(1)

NAME
       aewan - An ascii-art editor

SYNOPSIS
       aewan [filename]

DESCRIPTION
       Aewan  is  a  multi-layered ascii-art/animation editor that produces both stand-alone cat-
       able art files and an easy-to-parse format for integration in your  terminal  applications
       (for information about the file format, see the aewan(5) manpage).

       If  you invoke the program without a command-line argument, it will open an untitled docu‐
       ment for you to edit. If you supply an argument, it must be the name of an aewan file  you
       wish to open. That file must exist, or an error will occur.

PROJECT HOME PAGE
       The Aewan Project's official homepage is the following:

            http://aewan.sourceforge.net

       There  you  will find author information, FAQ, links to the latest version of the program,
       etc.

AUTHORS
       The developers can be contacted through the following e-mail address:

            aewan-devel@lists.sf.net

       People who have worked or are currently working on this project:

         * Bruno T. C. de Oliveira (brunotc@gmail.com)
         * Peep Pullerits (http://solicit.estprog.ee; solicit@estprog.ee)
         * Praveen Kurup <praveen_kurup@jasubhai.com>
         * Gerfried Fuchs <alfie@ist.org>

LICENSE INFORMATION
       Copyright (c) 2004 Bruno Takahashi C. de Oliveira. All rights reserved.

       This program is licensed under the GNU General Public  License,  version  2  or,  at  your
       option,  any later version. For full license information, please refer to the COPYING file
       that accompanies the program.

SEE ALSO
       aecat(1), aewan(5)

aewan (Aewan Ascii Art Editor)             August 2004                                   aewan(1)
