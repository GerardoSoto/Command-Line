TETRIS(6)                                BSD Games Manual                               TETRIS(6)

NAME
     tetris — the game of tetris

SYNOPSIS
     tetris [-ps] [-k keys] [-l level]

DESCRIPTION
     The tetris command runs display-based game which must be played on a CRT terminal.  The
     object is to fit the shapes together forming complete rows, which then vanish.  When the
     shapes fill up to the top, the game ends.  You can optionally select a level of play, or
     custom-select control keys.

     The default level of play is 2.

     The default control keys are as follows:

           j          move left
           k          rotate 1/4 turn counterclockwise
           l          move right
           ⟨space⟩    drop
           p          pause
           q          quit

     The options are as follows:

     -k      The default control keys can be changed using the -k option.  The keys argument must
             have the six keys in order, and, remember to quote any space or tab characters from
             the shell.  For example:

                   tetris -l 2 -k 'jkl pq'

             will play the default games, i.e. level 2 and with the default control keys.  The
             current key settings are displayed at the bottom of the screen during play.

     -l      Select a level of play.

     -s      Display the top scores.

     -p      Switch on previewing of the shape that will appear next.

PLAY
     At the start of the game, a shape will appear at the top of the screen, falling one square
     at a time.  The speed at which it falls is determined directly by the level: if you select
     level 2, the blocks will fall twice per second; at level 9, they fall 9 times per second.
     (As the game goes on, things speed up, no matter what your initial selection.)  When this
     shape “touches down” on the bottom of the field, another will appear at the top.

     You can move shapes to the left or right, rotate them counterclockwise, or drop them to the
     bottom by pressing the appropriate keys.  As you fit them together, completed horizontal
     rows vanish, and any blocks above fall down to fill in.  When the blocks stack up to the top
     of the screen, the game is over.

SCORING
     You get one point for every block you fit into the stack, and one point for every space a
     block falls when you hit the drop key.  (Dropping the blocks is therefore a good way to
     increase your score.)  Your total score is the product of the level of play and your accumu‐
     lated points -- 200 points on level 3 gives you a score of 600.  Each player gets at most
     one entry on any level, for a total of nine scores in the high scores file.  Players who no
     longer have accounts are limited to one score.  Also, scores over 5 years old are expired.
     The exception to these conditions is that the highest score on a given level is always kept,
     so that following generations can pay homage to those who have wasted serious amounts of
     time.

     The score list is produced at the end of the game.  The printout includes each player's
     overall ranking, name, score, and how many points were scored on what level.  Scores which
     are the highest on a given level are marked with asterisks “*”.

FILES
     /var/games/bsdgames/tetris-bsd.scores    high score file

BUGS
     The higher levels are unplayable without a fast terminal connection.

AUTHORS
     Adapted from a 1989 International Obfuscated C Code Contest winner by Chris Torek and Darren
     F. Provine.

     Manual adapted from the original entry written by Nancy L. Tinkham and Darren F. Provine.

     Code for previewing next shape added by Hubert Feyrer in 1999.

BSD                                        May 31, 1993                                       BSD
