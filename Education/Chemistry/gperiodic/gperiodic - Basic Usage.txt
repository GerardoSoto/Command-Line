The easiest thing to do would be to run the gperiodic script and then 
use CTRL+c to quit. However, you can look up elemental information by running:

    gperiodic Na
    
The above example displays information on the 'Na' (sodium) element. 
You have to use the abbreviations; you cannot use 'gperiodic hydrogen' 
and expect to get results.
