


                     Vic's QBasic Programming Tutorial

                           Basic Tutorial VII

                                Advanced
                            Sprite Movement


-----------------------------------------------------------------------------

This tutorial will cover how to move sprites on the screen in a more
proffesional manner...  This should of actually been in the last tutorial,
but, I think it would be more understanding to put it in its own little
section...

In my past programs I used to have to redraw the screen and then put the
sprite down.  When I wanted to move the sprite I had to erase the screen
and then redraw the background and then put the sprite down again...

If you have been doing this for a while, Stop...  It is a total waste of
time... Valuable time that is.  Qbasic is not a very fast language. If there
is any chance you have to speed it up, TAKE IT!

What If I had a background that was a screen full of pixels...
Here is an example of how long it would take to put it on the screen...

'----- Useless example start...

SCREEN 13
FOR y = 1 TO 200
FOR x = 1 TO 320
PSET (x, y), (RND * 255)
NEXT: NEXT

'--- Stop...

Now pretend that I had to redraw the background over and over for each frame.
That would take forever! It would have a FPS of .2!  That is not good for a
game... as if you didn't know...  How on earth can we fix that?  Well, the
first thing to know is that the background NEEDS to be redrawn.  But the
the thing is, you don't need to redraw the entire background, just what is
being drawn over.

How do we do that?
Easy, Before you put the sprite down you have to take a picture of the area
that it will cover.  Then you put the sprite down.  When you move the sprite
to a different place you put the picture that you took before you put the
sprite down in its place.  Then you take another picture of the place the
sprite will go next and then put the sprite down in the new new place...
If you look, the spot where the sprite was is totally gone!  You didn't even
have to redraw the screen!  Here is an example...  In this example I 
won't use the BIT BLT technique...  Just a quick example...


'--- Good example start...

DIM picture(1000), sprite(1000)

SCREEN 13
CIRCLE (10, 10), 10, 4
PAINT (10, 10), 1, 4
GET (0, 0)-(20, 20), sprite

CLS
FOR i = 1 TO 500
LINE ((RND * 320), (RND * 200))-((RND * 320), (RND * 200)), (RND * 255)
NEXT
LOCATE 1, 1: PRINT "Take a picture of the area that the sprite will go on..."

'     50 is where the sprite will go
'     Since the sprite is 20 x 20 it will get the screen from 50, 50
'     To 50 + 20, 50 + 20  ( 70, 70 )

GET (50, 50)-(70, 70), picture
GET (50, 50)-(70, 70), picture

SLEEP
LOCATE 1, 1: PRINT "Put the sprite down...                                  "
'Remember, the 50, 50 is where the sprite will go...
PUT (50, 50), sprite, PSET
SLEEP
LOCATE 1, 1: PRINT "Put the picture back down...                            "

PUT (50, 50), picture, PSET

SLEEP
LOCATE 1, 1: PRINT "Think of a new place to put the sprite and take a picture

'     70 , 50 is where the sprite will go now
'     Since the sprite is 20 x 20 it will get the screen from 70, 50
'     To 70 + 20, 50 + 20  ( 90, 70 )

LOCATE 1, 1: PRINT "Put the New sprite down...
PUT (70, 50), sprite, PSET

'--- STOP!


Did you see that? The sprite was erased but the screen was not redrawn
totally!  The only problem with this technique is that you have to
anticipate where the next sprite will be blaced and still keep trace of
where it was so you can erase it...

This is how you do that...

Before you think of a new place to put the sprite record it as another name
in another array...  like this...

x = 50    'Sprite coordinates X and Y
y = 50

get (x, y)-(x + sprite max x, y + sprite max y), picture of background
put (x,y),sprite      'Put the sprite down

oldx = x           'Record the old coords
oldy = y

x = x + 1          'get next place for the sprite
y = y + 1

put (oldx, oldy), picture of background

put (x,y), sprite

... Do it all over again until infinity...

Very easy, only you might not just think of that...

Lets try it in an example of using this technique...






'--- good good example start...

DIM picture(1000), sprite(1000)

SCREEN 13
CIRCLE (10, 10), 10, 4
PAINT (10, 10), 1, 4
GET (0, 0)-(20, 20), sprite

CLS
FOR i = 1 TO 500
LINE ((RND * 320), (RND * 200))-((RND * 320), (RND * 200)), (RND * 255)
NEXT

x = 1: y = 1           'Sprite X and Y coords
xadj = 1: yadj = 1     'Moves the ball
delay = 10000          'Adjust this for speed...

GET (x, y)-(x + 20, y + 20), picture  'Get the background

DO                'Start loop
press$ = INKEY$   'get ready for key presses...

PUT (x, y), sprite, PSET  ' put the sprite down

oldx = x           'Record old position
oldy = y

x = x + xadj      'Move the ball
y = y + yadj

WAIT &H3DA, 8      'This just keeps things clean, don't worry about it...

PUT (oldx, oldy), picture, PSET   'Put the old background down...

GET (x, y)-(x + 20, y + 20), picture   'Get The new background

PUT (x, y), sprite, PSET        'Finally, put the sprite down...

IF y > 170 THEN yadj = -1       'This is if it hits the walls...
IF y < 10 THEN yadj = 1
IF x > 290 THEN xadj = -1
IF x < 10 THEN xadj = 1

FOR i = 1 TO delay: NEXT       'This adds a delay, If its to fast or slow...

LOOP UNTIL press$ = CHR$(27)   'Loop until Escape is pressed...

'--- STOP...


Look at that...  I bet you didn't think it was possible in screen 13!
Thats all I had set for this tutorial...  I will leave you with one last
example... This example uses Bit BLT








'--- REALLY REALLY GOOD EXAMPLE START!

DIM picture(1000), sprite(1000), spritesh(1000)

SCREEN 13
CIRCLE (10, 10), 10, 4
PAINT (10, 10), 1, 4
CIRCLE (10, 10), 4, 4
PAINT (10, 10), 0, 4
GET (0, 0)-(20, 20), sprite
LINE (0, 0)-(20, 20), 255, BF
CIRCLE (10, 10), 10, 4
PAINT (10, 10), 1, 4
CIRCLE (10, 10), 4, 4
PAINT (10, 10), 255, 4

GET (0, 0)-(20, 20), spritesh
CLS
FOR i = 1 TO 500
LINE ((RND * 320), (RND * 200))-((RND * 320), (RND * 200)), (RND * 255)
NEXT

x = 1: y = 1           'Sprite X and Y coords
xadj = 1: yadj = 1     'Moves the ball
delay = 10000          'Adjust this for speed...

GET (x, y)-(x + 20, y + 20), picture  'Get the background

DO                'Start loop
press$ = INKEY$   'get ready for key presses...

PUT (x, y), spritesh, AND  ' put the sprite down
PUT (x, y), sprite, OR

oldx = x           'Record old position
oldy = y

x = x + xadj      'Move the ball
y = y + yadj

WAIT &H3DA, 8      'This just keeps things clean, don't worry about it...

PUT (oldx, oldy), picture, PSET   'Put the old background down...

GET (x, y)-(x + 20, y + 20), picture   'Get The new background

'PUT (x, y), sprite, PSET        'Finally, put the sprite down...

PUT (x, y), spritesh, AND  ' put the sprite down
PUT (x, y), sprite, OR



IF y > 170 THEN yadj = -1       'This is if it hits the walls...
IF y < 10 THEN yadj = 1
IF x > 290 THEN xadj = -1
IF x < 10 THEN xadj = 1

FOR i = 1 TO delay: NEXT       'This adds a delay, If its to fast or slow...

LOOP UNTIL press$ = CHR$(27)   'Loop until Escape is pressed...

'----  FINISH!!!!!!

I know how stupid a flying donut is, but what the heck...

-----------------------------------------------------------------------------
Thats it for this tutorial, If I didn't get into enough detail in the
explanations then just look at the source code and try to figure it out
on your own.  All else fails E-Mail Me...

My current E-Mail address is RADIOHANDS@AOL.com

If you are using this tutorial on your page, please leave the tutorial 
exactly as it is... please don't change anything, unless its spelling
errors... Theres alot of them! I don't like using the backspace key...

The original website that these were on is

http://members.aol.com/radiohands/index.html

Thank you
Vic Luce
Finished
November 10
1999

If you want to be notified when a new tutorial is out..
Send An E-mail to RADIOHANDS@AOL.com
with the subject saying VQBLIST and then your E-mail address(check website)

