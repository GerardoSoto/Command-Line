
                   Basic to Advance Keyboard Programming

                         x.t.r.GRAPHICS (TM)
                               Tutorails

-----------------------------------------------------------------------

    Basic Keyboard Programming:

    If you know how to use INKEY$ to use (A-Z/1-0/ and all the little
piriod, commas, and soforth) then go on to Advance Keyboard..

    You have probaly tried, just like me when I begain, to use the
following:

IF INKEY$ = "a" THEN GOTO...

    And as you found out, it doesn't work. Why? The best way I can look
at it is, the keyboard cycles. Here is a example: If you start a new
text document, and press and hold say A, what happens, a buch of a's
dash across the screen like its looping(OR: cycling).
    So if the keyboard cycles, then INKEY$ cycles. All I can figure
though is that the cycle is to fast for IF.. THEN. So we make a prompt
varible. Don't scream, it really simple:

kbp$ = INKEY$

    There, easy! now in the IF... THEN stament:

IF kbp$ = "a" THEN GOTO...

    Now it will work. kbp$ stands for Keyboard Press, you can use any
number of things like: press$, a$, key$, or whatever else you can think
of. Okay, now for a example:

'## Star Here: Program ###

DO
kbp$ = INKEY$                             ' Promt keypress
IF kbp$ = "a" THEN PLAY "L64 A"         ' Play note on keypress A
IF kbp$ = "q" THEN PLAY "L64 DBCA": END   ' This ends the program
LOOP                                      ' you can leave out the
                                          ' PLAY "L64 DBCA" if you like

'## Stop Here ###

    This works on (A - Z / 1 - 0 / and commas, piriods, and soforth)
Now if you got that, lets go on to Advance.

-----------------------------------------------------------------------

    Advance Keyboard Programing: 

    A - Z, 1 - 0, all fun, but how do you use the Esc,
arrow keys, F1 - F12, Enter, Insert, Home, Delete, End, Pgup, Pgdown, and Tab. I don't know them all yet, like Alt, Ctrl, and Shift. But I
know all the others I've listed.
    Esc, Enter, and Tab are strait foward CHR$() statments. As for
the others, they use a CHR$(0) + there string value. Here is a list...

KeyBoard Press List:

Esc = CHR$(27)
Enter = CHR$(13)
Tab = CHR$(9)

UpArrow = CHR$(0) + "H"
DownArrow = CHR$(0) + "P"
RightArrow = CHR$(0) + "M"
LeftArrow = CHR$(0) + "K"

F1 = CHR$(0) + ";"
F2 = CHR$(0) + "<"
F3 = CHR$(0) + "="
F4 = CHR$(0) + ">"
F5 = CHR$(0) + "?"
F6 = CHR$(0) + "@"
F7 = CHR$(0) + "A"
F8 = CHR$(0) + "B"
F9 = CHR$(0) + "C"
F10 = CHR$(0) + "D"
F11 = CHR$(0) + "�" *   (OR: CHR$(0) + CHR(133) )
F12 = CHR$(0) + "�" **  (OR: CHR$(0) + CHR(134) )

Insert = CHR$(0) + "R"
Home = CHR$(0) + "G"
Delete = CHR$(0) + "S"
PageUp = CHR$(0) + "I"
PageDown = CHR$(0) + "Q"

* Hold 'Alt' and type "1 3 3" to get '�'

** Hold 'Alt' and type "1 3 4" to get '�'
    
    These are the same as the CHR$() number codes in the ASCII chart.
    Holding 'Alt' while typing in the code number gives you any of the
    weird looking symbols in your code.

'End of List...

    I'd copy the list to a new text document and print it off, It will
make for easy reference. Now to apply the single CHR$ codes:

IF CHR$ = (27) THEN END ' This is the INCORRECT form...

IF kbp$ = CHR$(27) THEN END  ' This is the CORRECT form.

    The last one is correct as long as kbp$ = INKEY$. You can also
do it this way:

DO
kbp$ = INKEY$
LOOP UNTIL kbp$ = CHR$(27)

    You can use either of the two with the single (Esc, Tab, and Enter)
Here is a Enter press example with Esc:

'## Start Here: Program ###

DO
kbp$ = INKEY$
IF kbp$ = CHR$(13) THEN c = 10 ' Changes the c to 10 (Green) when you
COLOR c                        ' press 'Enter' 
LOCATE 1, 1: PRINT "You pressed Enter!!"     
LOOP UNTIL kbp$ = CHR$(27)     ' Ends LOOP when you press 'Esc'

'## Stop Here ###

    Now that we've got that, lets try the Double codes. We'll practic
with the arrow keys since that's probaly what you will want to use the
must of. Here is another example:

'## Start Here: Program ###

DO
kbp$ = INKEY$
IF kbp$ = CHR$(0) + "H" THEN stat$ = "UP"
IF kbp$ = CHR$(0) + "P" THEN stat$ = "DOWN"
IF kbp$ = CHR$(0) + "K" THEN stat$ = "LEFT"
IF kbp$ = CHR$(0) + "M" THEN stat$ = "RIGHT"
LOCATE 1, 1: PRINT stat$
LOOP UNTIL kbp$ = CHR$(27)

'## Stop Here ###

    That should get you started. Hope you have fun progaming!!

    If you wish to EMail me, to seperate from the spam I get,
    Place "QBasic Tutorial" in the subject. No attachments please.

EMAIL: Rattrapmax6@aol.com
WEBPAGE: http://hometown.aol.com/rattrapmax6/index.html

    