################################
# base64 - Using As Encryption #
################################

Encoding is not the same as "encrypting," but can be used to hide data if
needed in the form of a broken file. This is done by encoding a file into
base64 and then removing part of the code at a designed place. To decode
it back into a "healthy" file, you would add the code back in and save.
However, be aware that if you do this near the beginning, almost all
base64 encoding that I've noticed uses the same string up until character
number 14 when using the same file formats. THis probably has to do with
file headings. This does not seem to affect plain text file types.

Going even further...
---------------------

If you need to go even further than this, you can use what is called
'Stenography' to hide files inside of a JPEG image. Afterwards, you 
could then use the method mentioned above to make it look like a corrupted
file.

Encoding an image to base64:
---------------------------

    base64 "/path/to/file"
    
    *Use -w flag if you need word-wrapping in your terminal.
    
Decoding base64 to an image:
---------------------------

    base64 -d "iVBORw0KGgoAAA..."
