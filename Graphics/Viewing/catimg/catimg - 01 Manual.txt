CATIMG(1)                            General Commands Manual                            CATIMG(1)

NAME
       catimg - fast image printing in to your terminal

SYNOPSIS
       catimg [options] image

DESCRIPTION
       catimg  is  a  little  program written in C with no dependencies that prints images in the
       terminal. It supports JPEG, PNG and GIF formats.

OPTIONS
       -h     Prints a help message

       -w WIDTH
              Specify the width of the displayed image, by default catimg will use  the  terminal
              width.

       -l LOOPS
              Specify  the  amount  of  loops that catimg should repeat a GIF. A value of 1 means
              that the GIF will be displayed twice. A negative value implies infinity.

       -r RESOLUTION
              Possible values 1 or 2. Force the resolution of the image. By default  catimg  will
              check if rendering in higher resolution is possible and do so or use the lower one.

       -c     Convert colors to a restricted palette. This is useful when terminal only support a
              limited set of colors. This transformation should be more  accurate  than  the  one
              performed by the terminal.

BUGS
       Please report any bugs to https://github.com/posva/catimg/issues.

AUTHORS
       catimg was written by Eduardo San Martin Morote (https://github.com/posva)

       This manual page was written by Jonathan Carter <jcarter@linux.com> and Eduardo San Martin
       Morote

catimg                                     January 2017                                 CATIMG(1)
