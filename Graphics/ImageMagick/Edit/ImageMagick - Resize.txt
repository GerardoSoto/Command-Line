########################
# ImageMagick - Resize #
########################

Keeps aspect ratio:

    mogrify -resize WxH /path/to/image
    
If you want to force changing the aspect ratio:

    mogrify -resize WxH\! /path/to/image
