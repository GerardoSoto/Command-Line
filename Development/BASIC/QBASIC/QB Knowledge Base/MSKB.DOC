             Microsoft Knowledge Base Search/Retrieval Utility
                 Version 1.0, Copyright 1994 Scott MacLean
        Database contents Copyright 1988-1994 Microsoft Corporation

Overview
--------
     This program contains the entire Microsoft QuickBASIC knowledge base,
the same one used by Microsoft technical support when you call in with a
problem. Only documents dealing with Microsoft BASIC products are included in
this database, those being:

Microsoft Basic & QuickBASIC versions 1.0 to 4.5
Microsoft BASIC PDS versions 7.0 and 7.1
Microsoft BASIC for Macintosh, versions 1.00 to 1.00e

     The retrieval utility requires that you have a VGA card installed. There
is a very slight possibility that it may run with a CGA card, but you will
have to specify a different viewer (see below).

     Each knowledge base document contains a title, product application and
version, operating system applications and text. Fast searches can be done on
the title field, usually less than 5 seconds to search the entire database on
a 486DX2-66. Searching the entire text of all documents takes approximately
10-15 seconds on a 486DX2-66. Your mileage may vary. You can also retrieve
documents instantaneously if you know the document number.

Using the Program
-----------------
     To start the knowledge base retrieval utility, type the command MSKB.
You will be shown a main menu where you can select how you want to execute
the search. When the search is complete, you will be shown the headers of the
documents that were found, and you will have the option to view each and any
of the documents, or to save them to disk.

     If you have problems using the included viewing program, or you wish to
use a different one, specify it on the command line. You may wish to use the
DOS editor instead. This can be done with:

MSKB EDIT

     The documents in this system are current as of August 27, 1994. I will
issue periodic releases to keep up with any updates. If this program generates
favorable response, I may be persuaded to release similar programs containing
knowledge bases for other Microsoft products. Let me know.

Tech Stuff
----------
     This program is written in Microsoft BASIC PDS 7.1, using the internal
ISAM database routines, along with some hybrid search routines of my own. I
wrote this program in less than a day. If you wish, I am available to write
code to spec, on a contract basis. Contact me for more information.

The Cost
--------
     None. It's free. I suspect Microsoft would take a dim view of my
charging for redistributing their knowledge base, even though it is in the
public domain, and the retrieval program is mine. If you like it, I've got
lots more where it came from. Contact me for more information.

The Author
----------
I'm Scott MacLean. I am a professional programmer, and also an FAA licensed
commercial pilot, with single, multi & instrument priveleges. If you need
contract programming done, or have a plane for me to fly, contact me.

Voice: (301) 417-0939
BBS  : (301) 417-9341
