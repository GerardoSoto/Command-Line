##################################
# xmodmap - Left-Hand Your Mouse #
##################################

    xmodmap -e "pointer = 3 2 1"
    
To switch back to right-handed:

    xmodmap -e "pointer = 1 2 3"
