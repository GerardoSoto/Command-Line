################
# gpm - Basics #
################

gpm is essentially a cursor in TTY/console that you can move around using
your mouse for copy and pasting. You left-click and drag to highlight
to copy and then right-click to paste. If gpm doesn't start automatically
for you you can start it with this:

    sudo service gpm start
