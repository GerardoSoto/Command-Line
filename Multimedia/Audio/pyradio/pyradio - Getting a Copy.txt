############################
# pyradio - Getting a Copy #
############################

The easiest way to install pyradio is to:

1. Install MPV, Mplayer, or VLC
2. sudo pip install pyradio

You need to be able to type "mpv, mplayer, or cvlc" in a terminal and get
some kind of output for pyradio to work. If you're a Mac user, you can
get pyradio working by symbolically linking the binary inside of one of
the above players like so:

    ln -s /Applications/VLC.app/Contents/MacOS/VLC cvlc

You can also find the source code for those that don't have pip but Python
installed here:

    https://github.com/coderholic/pyradio
