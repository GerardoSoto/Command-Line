DAV(1)                               General Commands Manual                               DAV(1)

NAME
       Dav - A minimalist ncurses-based text editor.

DESCRIPTION
       A minimalist text editor.

SYNOPSIS
       dav [OPTIONS] [FILENAME] [FILENAME] ...

OPTIONS
       --version
              Display version information.

       --help Display usage information.

       -l[#]  Starts Dav at the line number [#].

FILENAME
       The names of the files, if any, that you wish to edit.

SETTINGS
       Personal settings are located in ~/.davrc

AUTHOR
       Written by David Gucwa <daverd@seanbaby.com>.

URL
       http://dav-text.sourceforge.net/

                                         February 7, 2002                                  DAV(1)
