################
# echo - Basic #
################

I put this in the Writing section because you can use echo to add text to
files like so:

    Replace all contents:
    --------------------
    
        echo "This text will replace everything" > file.txt
        
    Add text to last line of file:
    -----------------------------
    
        echo "This text goes to last line of file" >> file.txt

If the file does not exist and you use echo this way, it will create the
file, assuming that the full directory path actually exists.
