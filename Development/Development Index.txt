#####################
# Development Index #
#####################

base64 - base64 encode/decode data and print to standard output
bash - GNU Bourne-Again SHell
cc65 - 6502 C compiler (for old systems and game consoles)
dialog - display dialog boxes from shell scripts
git - the stupid content tracker (definition from actual man page)
hexedit - view and edit files in hexadecimal or in ASCII
markdown - convert text to html
ncurses - create text-based user interfaces for the terminal
octave - a high-level interactive language for numerical computations
parallel - run programs in parallel
pcbasic - a free, cross-platform emulator for the GW-BASIC family of interpreters
python - an interpreted, interactive, object-oriented programming language
shc - generic shell script compiler
yabasic - yet another Basic
