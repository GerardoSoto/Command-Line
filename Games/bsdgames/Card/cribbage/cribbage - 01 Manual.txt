CRIBBAGE(6)                              BSD Games Manual                             CRIBBAGE(6)

NAME
     cribbage — the card game cribbage

SYNOPSIS
     cribbage [-eqr]

DESCRIPTION
     cribbage plays the card game cribbage, with the program playing one hand and the user the
     other.  The program will initially ask the user if the rules of the game are needed – if so,
     it will print out the appropriate section from According to Hoyle with more(1).

     cribbage options include:

     -e      When the player makes a mistake scoring his hand or crib, provide an explanation of
             the correct score.  (This is especially useful for beginning players.)

     -q      Print a shorter form of all messages – this is only recommended for users who have
             played the game without specifying this option.

     -r      Instead of asking the player to cut the deck, the program will randomly cut the
             deck.

     cribbage first asks the player whether he wishes to play a short game ( “once around”, to
     61) or a long game ( “twice around”, to 121).  A response of ‘s’ will result in a short
     game, any other response will play a long game.

     At the start of the first game, the program asks the player to cut the deck to determine who
     gets the first crib.  The user should respond with a number between 0 and 51, indicating how
     many cards down the deck is to be cut.  The player who cuts the lower ranked card gets the
     first crib.  If more than one game is played, the loser of the previous game gets the first
     crib in the current game.

     For each hand, the program first prints the player's hand, whose crib it is, and then asks
     the player to discard two cards into the crib.  The cards are prompted for one per line, and
     are typed as explained below.

     After discarding, the program cuts the deck (if it is the player's crib) or asks the player
     to cut the deck (if it's its crib); in the latter case, the appropriate response is a number
     from 0 to 39 indicating how far down the remaining 40 cards are to be cut.

     After cutting the deck, play starts with the non-dealer (the person who doesn't have the
     crib) leading the first card.  Play continues, as per cribbage, until all cards are
     exhausted.  The program keeps track of the scoring of all points and the total of the cards
     on the table.

     After play, the hands are scored.  The program requests the player to score his hand (and
     the crib, if it is his) by printing out the appropriate cards (and the cut card enclosed in
     brackets).  Play continues until one player reaches the game limit (61 or 121).

     A carriage return when a numeric input is expected is equivalent to typing the lowest legal
     value; when cutting the deck this is equivalent to choosing the top card.

     Cards are specified as rank followed by suit.  The ranks may be specified as one of: ‘a’,
     ‘2’, ‘3’, ‘4’, ‘5’, ‘6’, ‘7’, ‘8’, ‘9’, ‘t’, ‘j’, ‘q’, and ‘k’, or alternatively, one of:
     ‘ace’, ‘two’, ‘three’, ‘four’, ‘five’, ‘six’, ‘seven’, ‘eight’, ‘nine’, ‘ten’, ‘jack’,
     ‘queen’, and ‘king’.  Suits may be specified as: ‘s’, ‘h’, ‘d’, and ‘c’, or alternatively
     as: ‘spades’, ‘hearts’, ‘diamonds’, and ‘clubs’.  A card may be specified as: “⟨rank⟩
     ⟨suit⟩”, or: “⟨rank⟩ of ⟨suit⟩”.  If the single letter rank and suit designations are used,
     the space separating the suit and rank may be left out.  Also, if only one card of the
     desired rank is playable, typing the rank is sufficient.  For example, if your hand was “2H,
     4D, 5C, 6H, JC, and KD” and it was desired to discard the king of diamonds, any of the fol‐
     lowing could be typed: ‘k’, ‘king’, ‘kd’, ‘k d’, ‘k of d’, ‘king d’, ‘king of d’, ‘k
     diamonds’, ‘k of diamonds’, ‘king diamonds’, ‘king of diamonds’.

FILES
     /usr/games/cribbage
     /var/games/bsdgames/criblog
     /usr/share/games/bsdgames/cribbage.instr

AUTHORS
     Earl T. Cohen wrote the logic.  Ken Arnold added the screen oriented interface.

BSD                                        May 31, 1993                                       BSD
