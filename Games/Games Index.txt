###############
# Games Index #
###############

alienwave - an ncurses-based space invaders clone
bsdgames - collection of classic textual unix games
cgames - cblocks, cmines, and csokoban
dosemu, xdosemu - run DOS and DOS programs under Linux
gnuchess - GNU Chess
gnugo - GNU program to play the game of Go
nettoe - a text based Tic Tac Toe game playable over the Internet
pacman4console - ncurses-based pacman game
tron - multiplayer tron game in your console via ssh
YOLO - a Seven Day Roguelike game
