From: https://www.cc65.org/links.php

Related Links                                                            [cc65]

  • Home
  • News
  • FAQ
  • Known bugs
  • Download
  • Links

All of the links below point to external pages. This means that I'm not
responsible for the contents. Please notify me if any of the links below is
broken, or if you want to have a specific link added.

Other 65xx compilers

Links to other compilers that produce code for some member of the 6502 family.

* http://www.monlynx.de/lynx/cc65.html

  This is another extended version of the original cc65 compiler targeted
  exclusively for the Atari Lynx. I did not have a look at the code until now,
  but I had some discussions with the author, and he has implemented some cool
  things like register variables.

* Quetzalcoatl

  Queztalcoatl is a Cross Compiler for 6502 Processors. Queztalcoatl runs under
  Win32 and Linux. It can compile and link programs written in a subset of ANSI
  C, Assembler, the 1983 UPL language or any combination thereof. It is
  suitable for producing programs for the Commodore VIC-20 and Commodore 64.

* Oric Software Delevopment Kit

  The OSDK is a collection of tools to create programs for the Oric line of
  6502 machines. It contains modified versions of the lcc C compiler, the xa
  cross assember and some other tools. As far as I can tell, no source code for
  the tools is available, but the package might be worth a look.

Information

Sites that provide information about compilers, the 6502 family of CPUs or
similar.

* cc65: A crosscompiler for the C64

  Rainer Buchty had a look at cc65 in his monthly GO64 column "World Watch",
  which is available online. The article is in german!

* www.6502.org

  A whole web site devoted to the popular 6502 CPU. Books, data sheets, lots of
  links and a source code repository - to name just a few contents.

* The Fridge

  Lots of 6502 code snippets, some of them are C64/Commodore specific. Well
  worth a look.

* 6502 Language Implementation Approaches

  Some information on implementing computer languages on the extremely old and
  obsolete 6502 chips.

C related

Links to pages covering the language C. Please note that these pages are not
cc65 specific.

* de.comp.lang.c FAQ

  Frequently asked questions (with answers) for the german usenet newsgroup
  de.comp.lang.c. The FAQ is in german language (as is the newsgroup).

* C programming

  A C programming tutorial written by Brian Brown.

* C programming reference

  By Martin Leslie. A german translation (by G. Junghanns) is also available.

Projects

Projects using the cc65 development tools.

* Chris Ward's 6502 computer system

  A homemade computer using the 6502 CPU. Chris has written the necessary
  library routines to support C programs on this system.

* uIP - A TCP/IP Implementation

  uIP is a small, free TCP/IP implementation for 8- and 16-bit
  microcontrollers. The 6502 version has been compiled and tested using cc65.

* The Contiki Operating System and Desktop Environment

  The Contiki desktop environment is a highly portable, modern, open source,
  Internet-enabled operating system and desktop environment for very small
  systems written in C by Adam Dunkels.

* NanoVM - a portable tiny java vm

  NanoVM is a portable tiny java vm running on microcontrollers with at least
  8k code space, especially Atmel AVR controllers. Robotic examples for the
  Asuro, the c't-Bot and the Nibo robot are included.

Tools

Tools that work together with cc65 in one way or the other.

* VICE

  When cross-developing programs for a 6502 machine, an emulator is a really
  nice thing. VICE emulates many different CBM computers and runs under Linux.
  Versions for other platforms are also available from the above web page (for
  more information about the VICE OS/2 port look here.)
  I'm often using VICE to debug new libraries or ports to new target machines.
  The ld65 linker is able to create a label file for VICE, so you see all label
  names of your program (even the local ones!) in the VICE machine language
  monitor.

* Prepend a load header

  Mu'ath Brown, <muath at engineer. com> has written a program to prepend a
  load header to any binary for use with Commodore systems. The program is
  quite portable, creating binaries for other operating systems shouldn't be a
  problem. Source code and a DOS executable are readily available.

* Binary format converter

  Some epromers or debuggers insist on Intel hex or Motorola S-Record files.
  SRecord is a program that converts files between these and many other
  formats.

* Small malloc debugging module

  MagerValp has written a small malloc wrapper module that does several sanity
  checks when allocating and freeing memory. Detects out-of-bounds writes,
  duplicate frees and several other common types of errors.

* RLE compression kit

  The RLE Toolkit consists of a small RLE compression library for cc65, command
  line utilities to pack and unpack files (usable on the host system), and
  sample code that shows you how to use it. The kit was written by MagerValp.

[ba] Back to the cc65 Main Page.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

Valid HTML 4.01! Valid CSS! [n] links.php; last change: 30-Aug-2011
Impressum
